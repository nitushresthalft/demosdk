package com.singularkey.base.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonSyntaxException
import com.gox.base.data.PreferencesHelper
import com.gox.base.data.PreferencesKey
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.singularkey.base.BuildConfig
import com.singularkey.base.data.NetworkError
import com.singularkey.base.data.model.BaseError
import com.singularkey.base.utils.SingularKeyErrorCode
import io.reactivex.disposables.CompositeDisposable
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class BaseViewModel<N> : ViewModel() {

    private var compositeDisposable = CompositeDisposable()
    private lateinit var mNavigator: WeakReference<N>
    private var liveErrorResponse = MutableLiveData<String>()

    var navigator: N
        get() = mNavigator.get()!!
        set(navigator) {
            this.mNavigator = WeakReference(navigator)
        }

    fun getCompositeDisposable() = compositeDisposable

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun getErrorObservable() = liveErrorResponse

    fun getErrorMessage(e: Throwable): BaseError? {
        var error = BaseError()
        when (e) {
            is JsonSyntaxException -> {
                if (BuildConfig.DEBUG)
                    error.message = e.message.toString()
                else
                    error.message = NetworkError.DATA_EXCEPTION
                error.statusCode = SingularKeyErrorCode.ERROR_INVALID_RESPONSE
            }
            is HttpException -> {
                //val responseBody = e.response().errorBody()
                /*if (e.code() == 401 && PreferencesHelper.get(
                        PreferencesKey.ACCESS_TOKEN, "")!! != ""
                ) {
                    //SessionManager.clearSession()
                }*/
                error.message = getError(e.response().errorBody()!!)
                error.statusCode = SingularKeyErrorCode.ERROR_UNAUTHORIZE
            }
            is SocketTimeoutException -> {
                error.message = NetworkError.TIME_OUT
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            is IOException -> {
                error.message = NetworkError.IO_EXCEPTION
                error.statusCode = SingularKeyErrorCode.ERROR_IO_EXCEPTION
            }
            is ConnectException -> {
                error.message = NetworkError.CONNECTION_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            is UnknownHostException -> {
                error.message = NetworkError.CONNECTION_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            else -> {
                error.message = NetworkError.SERVER_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
        }
        return error
    }

    private fun getError(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("message")
        } catch (e: Exception) {
            e.message!!
        }
    }
}
