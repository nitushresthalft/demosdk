package com.singularkey.base.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import java.lang.ref.WeakReference

class SingularKey private constructor(activity: Activity?, fragment: Fragment?) {
    private val _activity: WeakReference<Activity?> = WeakReference(activity)
    private val _fragment: WeakReference<Fragment?> = WeakReference(fragment)

    val fishBunContext: SingularKeyContext get() = SingularKeyContext()

    fun setCompanyId(companyId: String): SingularKeyBuilder {
        val singularTone = SingularKeyTone.apply {
            refresh()
            this.companyId = companyId
        }
        return SingularKeyBuilder(this, singularTone)
    }

    companion object {
        const val SINGULAR_KEY_REQUEST_CODE = 27
        const val INTENT_PATH = "intent_path"

        @JvmStatic
        fun from(activity: Activity) = SingularKey(activity, null)

        @JvmStatic
        fun from(fragment: Fragment) = SingularKey(null, fragment)
    }

    inner class SingularKeyContext {
        private val activity = _activity.get()
        private val fragment = _fragment.get()
        fun getContext(): Context =
            activity ?: fragment?.context ?: throw NullPointerException("Activity or Fragment Null")

        fun startActivityForResult(intent: Intent, requestCode: Int) {
            when {
                activity != null -> activity.startActivityForResult(intent, requestCode)
                fragment != null -> fragment.startActivityForResult(intent, requestCode)
                else -> throw NullPointerException("Activity or Fragment Null")
            }
        }
    }
}