package com.singularkey.base.base

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.singularkey.base.R
import com.singularkey.base.extensions.observeLiveData
import com.singularkey.base.views.CustomDialog

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {

    private val loadingLiveData = MutableLiveData<Boolean>()

    private lateinit var mViewDataBinding: T
    private lateinit var mParentView: View
    private lateinit var context: Context

    private lateinit var mCustomLoaderDialog: CustomDialog
    private lateinit var noInternetsnackBar: Snackbar

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    val loadingObservable: MutableLiveData<Boolean> get() = loadingLiveData

    protected abstract fun initView(mViewDataBinding: ViewDataBinding?)

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        super.onCreate(savedInstanceState)
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        initView(mViewDataBinding)
        context = this
        mCustomLoaderDialog = CustomDialog(this)
        noInternetsnackBar = Snackbar.make(
            window.decorView.rootView,
            getString(R.string.no_internet_connection),
            Snackbar.LENGTH_INDEFINITE
        )
        noInternetsnackBar.setActionTextColor(Color.BLUE)
        noInternetsnackBar.setTextColor(Color.WHITE)
        noInternetsnackBar.setBackgroundTint(Color.RED)
        val snackbarLayout: View = noInternetsnackBar.getView()
        val lp: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
// Layout must match parent layout type
// Layout must match parent layout type
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height: Int = displayMetrics.heightPixels
        lp.setMargins(50, height / 2, 50, 0)
// Margins relative to the parent view.
// This would be 50 from the top left.
// Margins relative to the parent view.
// This would be 50 from the top left.
        snackbarLayout.layoutParams = lp

        observeLiveData(loadingLiveData) { isShowLoading ->
            if (isShowLoading) showLoading() else hideLoading()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        if (noInternetsnackBar.isShown)
            hideNoInternetSnackBar()
    }

    private fun showLoading() {
        try {
            if (mCustomLoaderDialog.window != null) {
                mCustomLoaderDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                mCustomLoaderDialog.show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun hideLoading() = mCustomLoaderDialog.cancel()

    override fun onCreateView(
        parent: View?,
        name: String,
        context: Context,
        attrs: AttributeSet,
    ): View? {
        return super.onCreateView(parent, name, context, attrs)
        mParentView = window.decorView.findViewById(R.id.content)
    }

    protected fun setBindingVariable(variableId: Int, value: Any?) {
        mViewDataBinding.setVariable(variableId, value)
        mViewDataBinding.executePendingBindings()
    }

    protected fun openActivity(cls: Class<*>, shouldCloseActivity: Boolean) {
        startActivity(Intent(context, cls))
        if (shouldCloseActivity) finish()
    }

    protected fun openActivity(intent: Intent, shouldCloseActivity: Boolean) {
        startActivity(intent)
        if (shouldCloseActivity) finish()
    }

    protected fun replaceFragment(
        @IdRes id: Int,
        fragment: Fragment,
        tag: String?,
        addToBackStack: Boolean,
    ) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(id, fragment, tag)
        if (addToBackStack) transaction.addToBackStack(tag)
        transaction.commit()
    }

    private fun showSnackBar(msg: String) {
        val snackBar = Snackbar.make(window.decorView.rootView, msg, Snackbar.LENGTH_LONG)
        snackBar.setActionTextColor(Color.RED)
        snackBar.show()
    }

    private fun showNoInternetSnackBar() {
        noInternetsnackBar.show()
    }

    private fun hideNoInternetSnackBar() {
        noInternetsnackBar.dismiss()
    }

    fun longLog(str: String, s: String) {
        if (str.length > 4000) {
            Log.d("$s::Points : ", str.substring(0, 4000))
            longLog(str.substring(4000), s)
        } else Log.d("$s::Points : ", str)
    }

    fun glideSetImageView(imageView: ImageView, image: String, placeholder: Int) {
        Glide.with(this)
            .applyDefaultRequestOptions(
                RequestOptions()
                    .placeholder(placeholder)
                    .circleCrop()
                    .error(placeholder)
            )
            .load(image)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imageView)
    }

    fun glideSetImageView(imageView: ImageView, uri: Uri, placeholder: Int) {
        Glide.with(this)
            .applyDefaultRequestOptions(
                RequestOptions()
                    .placeholder(placeholder)
                    .circleCrop()
                    .error(placeholder)
            )
            .load(uri)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imageView)
    }

    fun showSuccess(msg: String) {
        showSnackBar(msg)
    }

    fun showError(msg: String) {
        showSnackBar(msg)
    }
}