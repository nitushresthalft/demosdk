package com.singularkey.base.base

interface CustomizeProperty {
    fun setActionBarColor(actionbarColor: Int): SingularKeyBuilder

    fun setActionBarTitleColor(actionbarTitleColor: Int): SingularKeyBuilder
    fun setStatusBarColor(statusBarColor: Int): SingularKeyBuilder

    fun setUserName(userName: String): SingularKeyBuilder

    fun setFlowId(flowId: String?): SingularKeyBuilder
    fun setPolicyId(policyId: String?): SingularKeyBuilder
    fun setAccessToken(accessToken: String): SingularKeyBuilder
    fun startAuthenticationFlow()
}