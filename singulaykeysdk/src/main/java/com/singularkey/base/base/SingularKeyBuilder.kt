package com.singularkey.base.base

import android.content.Intent
import com.singularkey.base.ui.mfacontainer.SingularKeyActivity

class SingularKeyBuilder(
    private val singularKey: SingularKey,
    private val singularTone: SingularKeyTone,
) : CustomizeProperty {

    private var requestCode = SingularKey.SINGULAR_KEY_REQUEST_CODE

    override fun setActionBarColor(actionbarColor: Int): SingularKeyBuilder = this.apply {
        singularTone.colorActionBar = actionbarColor
    }

    override fun setActionBarTitleColor(actionbarTitleColor: Int): SingularKeyBuilder = this.apply {
        singularTone.colorActionBarTitle = actionbarTitleColor
    }

    override fun setStatusBarColor(statusBarColor: Int): SingularKeyBuilder = this.apply {
        singularTone.colorStatusBar = statusBarColor
    }

    override fun setUserName(userName: String): SingularKeyBuilder = this.apply {
        singularTone.userName = userName
    }

    override fun setFlowId(flowId: String?): SingularKeyBuilder = this.apply {
        singularTone.flowId = flowId
    }

    override fun setPolicyId(policyId: String?): SingularKeyBuilder = this.apply {
        singularTone.policyId = policyId
    }

    override fun setAccessToken(accessToken: String): SingularKeyBuilder = this.apply {
        singularTone.accessToken = accessToken
    }

    override fun startAuthenticationFlow() {
        val fishBunContext = singularKey.fishBunContext
        val context = fishBunContext.getContext()

        if (singularTone.companyId == null) throw NullPointerException("Company Id is Null")

        with(singularTone) {
//TODO set default data
        }

        val intent =
            Intent(context, SingularKeyActivity::class.java)
        intent.putExtra(SingularKeyActivity.USER_NAME, singularTone.userName)
        intent.putExtra(SingularKeyActivity.TOKEN, singularTone.accessToken)
        intent.putExtra(SingularKeyActivity.COMPANY_ID, singularTone.companyId)
        intent.putExtra(SingularKeyActivity.FLOW_ID, singularTone.flowId)
        intent.putExtra(SingularKeyActivity.POLICY_ID, singularTone.policyId)
        fishBunContext.startActivityForResult(intent, requestCode)
    }
}