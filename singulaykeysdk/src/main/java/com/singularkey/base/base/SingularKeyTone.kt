package com.singularkey.base.base

import android.graphics.Color

object SingularKeyTone {
    var colorActionBar: Int = 0
    var colorActionBarTitle: Int = 0
    var colorStatusBar: Int = 0

    var companyId: String? = null
    var userName: String? = null
    var accessToken: String? = null
    var flowId: String? = null
    var policyId: String? = null

    var colorAccent: Int = 0

    init {
        initValue()
    }

    fun refresh() = initValue()

    private fun initValue() {
        colorActionBar = Color.parseColor("#9bc754")
        colorActionBarTitle = Color.parseColor("#ffffff")
        colorStatusBar = Color.parseColor("#cde3a8")
        colorAccent = Color.parseColor("#9bc754")

        userName = null
        accessToken = null
        flowId = null
        policyId = null
        companyId = null


    }
}