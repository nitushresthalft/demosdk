package com.singularkey.base.session

object SessionManager {

    private lateinit var listener: SessionListener

    fun instance(listener: SessionListener){
        SessionManager.listener = listener
    }

    fun clearSession(){
        listener.invalidate()
    }


}