package com.singularkey.base.session

interface SessionListener{
    fun invalidate()
    fun refresh()
}