package com.singularkey.base.repository

/**
 * Listener class for all the api response
 * */
interface ApiListener {
    fun onSuccess(successData: Any)
    fun onError(error: Throwable)
}