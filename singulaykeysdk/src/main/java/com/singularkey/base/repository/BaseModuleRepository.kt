package com.singularkey.base.repository

import com.singularkey.base.data.model.SingularKeyRegistrationCompleteRequest
import com.singularkey.base.data.model.RegistrationInitiationRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Repository class that makes api calls.
 * */
class BaseModuleRepository : BaseRepository() {

    /**
     * Method to call start flow/policy api
     *
     * @param company_id: Id of the company
     * @param policy_id: value of policy id
     * @param flow_id: flow id value
     * @param token: auth token you received from server
     * @param username: User name entered by user
     *@param listener: call back listener for api request
     *  on which calling class will be listening
     *
     * */
    fun startFlow(
        company_id: String?,
        policy_id: String?,
        flow_id: String?,
        token: String,
        username: String,
        listener: ApiListener,
    ): Disposable {
        if (flow_id != null) {
            return BaseRepository().createApiClient(
                SingularKeyApiConstants.DEV_API_BASE_URL,
                BaseWebService::class.java
            )
                .startFlowWithFlowId(company_id!!, flow_id, "Bearer " + token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    listener.onSuccess(it)
                }, {
                    it.printStackTrace()
                    listener.onError(it)
                })
        } else {
            return BaseRepository().createApiClient(
                SingularKeyApiConstants.DEV_API_BASE_URL,
                BaseWebService::class.java
            )
                .startFlowWithPolicyId(company_id!!, policy_id!!, "Bearer " + token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    listener.onSuccess(it)
                }, {
                    it.printStackTrace()
                    listener.onError(it)
                })
        }
    }

    /**
     * Method to initialize the authentication process with api call
     *
     * @param token: Auth token received from server
     * @param interactionId: Interaction id received from start flow/policy api
     * @param companyId: Id of the company
     * @param connectionId: Connection id received from start flow/policy api
     * @param capabilityName: Capability name received from start flow/policy api
     * @param request: RegistrationIntitiationRequest object
     * @param listener: api response listener which the calling class will be listening
     * */
    fun initAuthRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
        listener: ApiListener,
    ): Disposable {
        return BaseRepository().createApiClient(
            SingularKeyApiConstants.DEV_API_BASE_URL,
            BaseWebService::class.java
        ).registerForAuthentication(token,
            companyId,
            connectionId,
            capabilityName,
            interactionId,
            request)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                listener.onSuccess(it)
            }, {
                it.printStackTrace()
                listener.onError(it)
            })
    }

    /**
     * Method to complete the authentication process with api call
     *
     * @param token: Auth token received from server
     * @param interactionId: Interaction id received from start flow/policy api
     * @param companyId: Id of the company
     * @param connectionId: Connection id received from start flow/policy api
     * @param capabilityName: Capability name received from start flow/policy api
     * @param request: RegistrationIntitiationRequest object
     * @param listener: api response listener which the calling class will be listening
     * */
    fun completeAuthRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
        listener: ApiListener,
    ): Disposable {
        return BaseRepository().createApiClient(
            SingularKeyApiConstants.DEV_API_BASE_URL,
            BaseWebService::class.java
        ).registerForAuthenticationComplete(
            token,
            companyId,
            connectionId,
            capabilityName,
            interactionId,
            request
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                listener.onSuccess(it)
            }, {
                it.printStackTrace()
                listener.onError(it)
            })
    }

    companion object {
        private var appRepository: BaseModuleRepository? = null

        fun instance(): BaseModuleRepository {
            if (appRepository == null) synchronized(BaseModuleRepository) {
                appRepository = BaseModuleRepository()
            }
            return appRepository!!
        }
    }
}
