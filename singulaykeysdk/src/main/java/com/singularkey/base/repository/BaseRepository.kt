package com.singularkey.base.repository

import com.google.gson.JsonSyntaxException
import com.gox.base.data.PreferencesHelper
import com.gox.base.data.PreferencesKey
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.singularkey.base.BaseApplication
import com.singularkey.base.BuildConfig
import com.singularkey.base.data.NetworkError
import com.singularkey.base.data.model.BaseError
import com.singularkey.base.utils.SingularKeyErrorCode
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Base Repository Module
 *
 * **/
@Singleton
open class BaseRepository {

    @Inject
    lateinit var retrofit: Retrofit

    init {
        BaseApplication().baseComponent.inject(this)
    }

    @Singleton
    fun reconstructedRetrofit(baseUrl: String): Retrofit {
        return retrofit.newBuilder()
            .baseUrl("$baseUrl")
            .build()
    }

    fun <T> createApiClient(service: Class<T>): T {
        return retrofit.create(service)
    }

    fun <T> createApiClient(baseUrl: String, service: Class<T>): T {
        println("create api client url $baseUrl")
        return reconstructedRetrofit(baseUrl).create(service)
    }

    /**
     * Method to parse the error received from server
     *
     * @param e: takes throwable data as input
     *
     * @return BaseError: send error in BaseError Model, received after parsing the error
     *
     * */
    fun getErrorMessage(e: Throwable): BaseError? {
        var error = BaseError()
        when (e) {
            is JsonSyntaxException -> {
                if (BuildConfig.DEBUG)
                    error.message = e.message.toString()
                else
                    error.message = NetworkError.DATA_EXCEPTION
                error.statusCode = SingularKeyErrorCode.ERROR_INVALID_RESPONSE
            }
            is HttpException -> {
                //val responseBody = e.response().errorBody()
                /*if (e.code() == 401 && PreferencesHelper.get(
                        PreferencesKey.ACCESS_TOKEN, "")!! != ""
                ) {
                    //SessionManager.clearSession()
                }*/
                error.message = parseError(e.response())
                error.statusCode = SingularKeyErrorCode.ERROR_UNAUTHORIZE
            }
            is SocketTimeoutException -> {
                error.message = NetworkError.TIME_OUT
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            is IOException -> {
                error.message = NetworkError.IO_EXCEPTION
                error.statusCode = SingularKeyErrorCode.ERROR_IO_EXCEPTION
            }
            is ConnectException -> {
                error.message = NetworkError.CONNECTION_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            is UnknownHostException -> {
                error.message = NetworkError.CONNECTION_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
            else -> {
                error.message = NetworkError.SERVER_EXCEPTION
                error.statusCode = SingularKeyErrorCode.NETWORK_ERROR
            }
        }
        return error
    }

    /**
     * Parse error response returned by retrofit
     *
     * @param response: response api call via retrofit
     *
     * @return String: Error Message got after parsing the data/response
     *
     * */
    fun parseError(response: Response<*>): String {
        val converter: Converter<ResponseBody?, BaseError> =
            retrofit.responseBodyConverter(BaseError::class.java,
                arrayOfNulls<Annotation>(0))
        val error: BaseError?
        error = try {
            converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
            return e.localizedMessage!!
        }
        if (error != null) {
            if (error.code != null)
                return error.code!! + " : " + error.message!!
            else return error.message!!
        } else {
            return "Unknown Error"
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String? {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("message")
        } catch (e: Exception) {
            e.message
        }
    }

}
