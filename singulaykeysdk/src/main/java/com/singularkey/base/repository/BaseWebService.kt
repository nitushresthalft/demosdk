package com.singularkey.base.repository

import com.singularkey.base.data.model.*
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Interface class that contains all the api call method
 *
 * */
interface BaseWebService {

    @GET(SingularKeyApiConstants.POLICY_FLOW_START_URL)
    fun startFlowWithPolicyId(
        @Path("company_id") company_id: String,
        @Path("policy_id") policy_id: String,
        @Header("Authorization") token: String,
    ): Observable<Flow>

    @GET(SingularKeyApiConstants.FLOW_START_URL)
    fun startFlowWithFlowId(
        @Path("company_id") company_id: String,
        @Path("flow_id") flow_id: String,
        @Header("Authorization") token: String,
    ): Observable<Flow>

    @POST(SingularKeyApiConstants.FIDOInitRegister)
    fun registerForAuthentication(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String,
        @Path("connectionId") connectionId: String,
        @Path("capabilityname") capabilityName: String,
        @Query("interactionId") interactionId: String,
        @Body request: RegistrationInitiationRequest,
    ): Observable<AuthenticationRegistrationBaseResponse>

    @POST(SingularKeyApiConstants.FIDOInitRegister)
    fun registerForAuthenticationComplete(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String,
        @Path("connectionId") connectionId: String,
        @Path("capabilityname") capabilityName: String,
        @Query("interactionId") interactionId: String,
        @Body request: SingularKeyRegistrationCompleteRequest,
    ): Observable<AuthenticationRegistrationSuccess>

}
