package com.singularkey.base.repository

/**
 *
 * Class containing all the urls need for api call
 *
 * */
class SingularKeyApiConstants {
    companion object {
        const val BASE_URL = "https://devapi.singularkey.com/v1/"

        //const val BASE_URL = "https://56f7878b3774.ngrok.io/v1/"
        val PASSWORD_LESS_BASE_URL = "https://4976c7f1ed6a.ngrok.io/"

        const val DEV_API_BASE_URL = "https://devapi.singularkey.com/v1/"
        //const val DEV_API_BASE_URL = "https://56f7878b3774.ngrok.io/v1/"

        const val PASSWORD_LESS_REGISTER = "passwordlessregister"
        const val PASSWORD_LESS_LOGIN = "passwordlesslogin"
        const val USER_INITIATE = "users"

        //Flow endpoint
        const val FLOW_START_URL = "auth/{company_id}/flows/{flow_id}/start"
        const val POLICY_FLOW_START_URL = "auth/{company_id}/policy/{policy_id}/start"

        //SDK Token path
        const val SDK_TOKEN_URL = "users/{user_id}/sdktoken"

        const val RP_SERVER_URL =
            "https://4976c7f1ed6a.ngrok.io/"  //e.g., https://api.singularkey.com
        const val RPID = "4976c7f1ed6a.ngrok.io"

        const val FIDOInitRegister =
            "auth/{companyId}/connections/{connectionId}/capabilities/{capabilityname}"
    }
}