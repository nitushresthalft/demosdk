package com.singularkey.base.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * Class to create a custom view like textview, edittext, linear layout and so on
 */
public class CustomView {

    /**
     * Builder class to build linear layout with user defined attributes
     */
    public static class LinearLayoutBuilder {
        public Context context;
        private LinearLayout view;
        private GradientDrawable drawable;

        /**
         * Constructor for LinearLayout Builder class.
         *
         * @param context: Application context
         **/
        public LinearLayoutBuilder(Context context) {
            this.context = context;
            this.view = new LinearLayout(context);
        }

        /**
         * Method to set background of Linear Layout
         *
         * @param background: color value set by user.
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setBackground(int background) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setColor(background);
            return this;
        }

        /**
         * Method to set gravity of child view of Linear Layout
         *
         * @param gravity: gravity set by user (can be center, center_horizontal and so on).
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setGravity(int gravity) {
            this.view.setGravity(gravity);
            return this;
        }

        /**
         * Method to set padding of Linear Layout
         *
         * @param left:   left padding value.
         * @param right:  right padding value.
         * @param top:    top padding value.
         * @param bottom: bottom padding value.
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setPadding(int left, int top, int right, int bottom) {
            this.view.setPadding(left, top, right, bottom);
            return this;
        }

        /**
         * Method to add layout parameter
         *
         * @param params: Layout parameters for Linear layout
         * @return LinearLayoutBuilder: return instance of linear layout builder
         */
        public LinearLayoutBuilder setLayoutParam(ViewGroup.LayoutParams params) {
            this.view.setLayoutParams(params);
            return this;
        }

        /**
         * Method to set tag which act as parameter that will be sent to server
         *
         * @param tag: the field name for parameter
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setTag(String tag) { //Will act as parameters to be sent to server
            this.view.setTag(tag);
            return this;
        }


        /**
         * Method to set corner radius of layout
         *
         * @param cornerRadius: round corner value
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setCornerRadius(int cornerRadius) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setCornerRadius(cornerRadius);
            return this;
        }

        /**
         * Method to set stroke color of layout
         *
         * @param stroke: parsed color value
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setStrokeColor(int stroke) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setStroke(1, stroke);
            return this;
        }

        /**
         * Method to set orientation to add child view in layout
         *
         * @param orientation: orientation value of Linear layout (i.e. horizontal, vertical)
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public LinearLayoutBuilder setOrientation(int orientation) {
            this.view.setOrientation(orientation);
            return this;
        }

        /**
         * Method to build the layout by adding background drawable to the view
         *
         * @return LinearLayout: returns the linear layout
         * by adding all the use define attribute values
         */
        public LinearLayout build() {
            if (drawable != null) {
                this.view.setBackgroundDrawable(drawable);
            }
            return this.view;
        }
    }

    /**
     * Layout builder class for Textview which will take user define attributes for text view
     */
    public static class TextViewBuilder {
        public Context context;
        private AppCompatTextView textView;
        private GradientDrawable drawable;

        /**
         * constructor for text view builder class
         *
         * @param context : takes application context
         */

        public TextViewBuilder(Context context) {
            this.context = context;
            this.textView = new AppCompatTextView(context);
        }

        /**
         * Method to set text color of text view
         *
         * @param color: Parsed color value..
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setTextColor(int color) {
            this.textView.setTextColor(color);
            return this;
        }

        /**
         * Method to set hint color of text view
         *
         * @param color: Parsed value of color
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setTextHintColor(int color) {
            this.textView.setHintTextColor(color);
            return this;
        }

        /**
         * Method to set size of text in text view
         *
         * @param size : Size value for text
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setTextSize(float size) {
            this.textView.setTextSize(size);
            return this;
        }

        /**
         * Method to set the appearance of the text in text view
         *
         * @param textAppearance: It takes the text appearance like small, medium large...
         *                        attribute that is supported by text view
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setTextAppearence(int textAppearance) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                this.textView.setTextAppearance(context, textAppearance);
            } else {
                this.textView.setTextAppearance(textAppearance);
            }
            return this;
        }

        /**
         * Method to set background drawable color of textview
         *
         * @param background: Parsed color value for background
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setBackground(int background) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setColor(background);
            return this;
        }

        /**
         * Method to set text/string that needs to be displayed to user
         *
         * @param text: string to be displayed in texview
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setText(String text) {
            this.textView.setText(text);
            return this;
        }

        /**
         * Method to set text/string input type
         *
         * @param type: input type in textview
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setInputType(int type) {
            this.textView.setInputType(type);
            return this;
        }

        /**
         * Method to set text/string that needs to be displayed to user
         *
         * @param hint: hint string to be displayed
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setHintText(String hint) {
            this.textView.setHint(hint);
            return this;
        }

        /**
         * Method to set icon inside text view
         *
         * @param drawable: icon/drawable to be displayed in left side/start of text view
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setLeftDrawable(int drawable) {
            this.textView.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
            return this;
        }

        /**
         * Method to set icon inside text view
         *
         * @param drawable: icon/drawable to be displayed in right side/end of text view
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setRightDrawable(int drawable) {
            this.textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
            return this;
        }

        /**
         * Method to set layout params of text view i.e, height and width, margins etc
         *
         * @param params: layout parameters to be set to text view
         * @return TextViewBuilder: return instance of text view builder
         */
        public TextViewBuilder setLayoutParam(ViewGroup.LayoutParams params) {
            this.textView.setLayoutParams(params);
            return this;
        }

        /**
         * Method to set padding of text view
         *
         * @param left:   left padding value.
         * @param right:  right padding value.
         * @param top:    top padding value.
         * @param bottom: bottom padding value.
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setPadding(int left, int top, int right, int bottom) {
            this.textView.setPadding(left, top, right, bottom);
            return this;
        }

        /**
         * Method to set type face of text for text view
         *
         * @param face: typeface data for text it can be font, font style
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setTypeFace(Typeface face) {
            this.textView.setTypeface(face);
            return this;
        }

        /**
         * Method to set tag which act as parameter that will be sent to server
         *
         * @param tag: the field name for parameter
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setTag(String tag) { //Will act as parameters to be sent to server
            this.textView.setTag(tag);
            return this;
        }

        /**
         * Method to set gravity to text in text view i.e left/start, right/end, center,
         * center_horizontal, center_vertical
         *
         * @param gravity: gravity value
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setGravity(int gravity) {
            this.textView.setGravity(gravity);
            return this;
        }

        /**
         * Method to set corner radius of background of text view
         *
         * @param cornerRadius: radius value
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setCornerRadius(int cornerRadius) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setCornerRadius(cornerRadius);
            return this;
        }

        /**
         * Method to set border stroke color of text view
         *
         * @param stroke: stroke color in parsed color value
         * @return TextViewBuilder: return instance of text view builder
         **/
        public TextViewBuilder setStrokeColor(int stroke) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setStroke(1, stroke);
            return this;
        }

        /**
         * Method to build the text view after setting user defined attributes
         *
         * @return LinearLayoutBuilder: return instance of linear layout builder
         **/
        public AppCompatTextView build() {
            if (drawable != null) {
                this.textView.setBackgroundDrawable(drawable);
            }
            return this.textView;
        }
    }

    /**
     * Layout builder class for Edit Text which will take user define attributes for edit text view
     */
    public static class EditTextBuilder {
        public Context context;
        private AppCompatEditText view;
        private GradientDrawable drawable;

        /**
         * constructor for edit text view builder class
         *
         * @param context : takes application context
         */
        public EditTextBuilder(Context context) {
            this.context = context;
            this.view = new AppCompatEditText(context);
        }

        /**
         * Method to set text color of edit text view
         *
         * @param color: Parsed color value..
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setTextColor(int color) {
            this.view.setTextColor(color);
            return this;
        }

        /**
         * Method to set text color of hint for edit text
         *
         * @param color: Parsed color value..
         * @return EditTextBuilder: return instance of edit text builder
         */

        public EditTextBuilder setTextHintColor(int color) {
            this.view.setHintTextColor(color);
            return this;
        }

        /**
         * Method to set text size of text for edit text
         *
         * @param size: size value for text
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setTextSize(float size) {
            this.view.setTextSize(size);
            return this;
        }

        /**
         * Method to set appearance of text for edit text
         *
         * @param textAppearance: text appearance value can be small,medium or large
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setTextAppearence(int textAppearance) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                this.view.setTextAppearance(context, textAppearance);
            } else {
                this.view.setTextAppearance(textAppearance);
            }
            return this;
        }

        /**
         * Method to set color of background drawable for edit text background
         *
         * @param background: parsed value of color to be set as background color
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setBackground(int background) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setColor(background);
            return this;
        }

        /**
         * Method to set text to be visible in edit text
         *
         * @param text: string to be insert in edit text
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setText(String text) {
            this.view.setText(text);
            return this;
        }

        /**
         * Method to set input type of string that is going to be entered by user.
         *
         * @param type: input type value like. email, password etc
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setInputType(int type) {
            this.view.setInputType(type);
            return this;
        }

        /**
         * Method to set hint for edit text
         *
         * @param hint: string that needs to be set as hint
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setHintText(String hint) {
            this.view.setHint(hint);
            return this;
        }

        /**
         * Method to set icon on left/ start of edit text
         *
         * @param drawable: id of drawable
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setLeftDrawable(int drawable) {
            this.view.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
            return this;
        }

        /**
         * Method to set padding for edit text
         *
         * @param left:   left padding value
         * @param right:  right padding value
         * @param top:    top padding value
         * @param bottom: bottom padding value
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setPadding(int left, int top, int right, int bottom) {
            this.view.setPadding(left, top, right, bottom);
            return this;
        }

        /**
         * Method to set icon on right side/end of edit text
         *
         * @param drawable: id of drawable
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setRightDrawable(int drawable) {
            this.view.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
            return this;
        }

        /**
         * Method to set layout parameters for edit text
         *
         * @param params: layout params to be set on edit text ie. height, width, margin and so on
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setLayoutParam(ViewGroup.LayoutParams params) {
            this.view.setLayoutParams(params);
            return this;
        }

        /**
         * Method to set font or font style of text of edit text
         *
         * @param face:type face value of font or font style canbe user defined font family or system's font style
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setTypeFace(Typeface face) {
            this.view.setTypeface(face);
            return this;
        }

        /**
         * Method to set tag to edit text
         *
         * @param tag: tag that act as field/variable name for request sent to server
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setTag(String tag) { //Will act as parameters to be sent to server
            this.view.setTag(tag);
            return this;
        }

        /**
         * Method to set corner radius of edit text
         *
         * @param cornerRadius:radius value
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setCornerRadius(int cornerRadius) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setCornerRadius(cornerRadius);
            return this;
        }

        /**
         * Method to set stroke color of background if any for edit text
         *
         * @param stroke: parsed value of color
         * @return EditTextBuilder: return instance of edit text builder
         */
        public EditTextBuilder setStrokeColor(int stroke) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setStroke(1, stroke);
            return this;
        }

        /**
         * Method to build and return the edit text with user defined attributes
         *
         * @return AppCompatEditText: return desired edit text
         */
        public AppCompatEditText build() {
            if (drawable != null) {
                this.view.setBackgroundDrawable(drawable);
            }
            return this.view;
        }
    }

    /**
     * Builder class to build button with user defined attributes
     */
    public static class ButtonBuilder {
        public Context context;
        private AppCompatButton view;
        private GradientDrawable drawable;

        /**
         * constructor for button view builder class
         *
         * @param context : takes application context
         */
        public ButtonBuilder(Context context) {
            this.context = context;
            this.view = new AppCompatButton(context);
        }

        /**
         * Method to set text color of button text
         *
         * @param color: Parsed color value..
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTextColor(int color) {
            this.view.setTextColor(color);
            return this;
        }

        /**
         * Method to set text color of hint for button
         *
         * @param color: Parsed color value..
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTextHintColor(int color) {
            this.view.setHintTextColor(color);
            return this;
        }

        /**
         * Method to set size of text to be displayed on button
         *
         * @param size: size value for text
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTextSize(float size) {
            this.view.setTextSize(size);
            return this;
        }

        /**
         * Method to set text appearance of text that will be displayed on button
         *
         * @param textAppearance: TextAppearance value can be small, medium and large
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTextAppearence(int textAppearance) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                this.view.setTextAppearance(context, textAppearance);
            } else {
                this.view.setTextAppearance(textAppearance);
            }
            return this;
        }


        /**
         * Method to set background color of button
         *
         * @param background: Parsed color value..
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setBackground(int background) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setColor(background);
            return this;
        }

        /**
         * Method to set text to be displayed on button
         *
         * @param text: String/label that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setText(String text) {
            this.view.setText(text);
            return this;
        }

        /**
         * Method to set space between button icon and text on button
         *
         * @param padding: int value of space
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setDrawablePadding(int padding) {
            this.view.setCompoundDrawablePadding(padding);
            return this;
        }

        /**
         * Method to set icon on left side of button
         *
         * @param drawable: id value of drawable that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setLeftDrawable(int drawable) {
            this.view.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
            return this;
        }

        /**
         * Method to set icon on right side of button
         *
         * @param drawable: id value of drawable that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setRightDrawable(int drawable) {
            this.view.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
            return this;
        }

        /**
         * Method to set drawable on left side of button
         *
         * @param drawable: drawable that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setLeftDrawableIcon(Drawable drawable) {
            this.view.setCompoundDrawables(drawable, null, null, null);
            return this;
        }

        /**
         * Method to set icon on right side of button
         *
         * @param drawable: id value of drawable that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setRightDrawableIcon(int drawable) {
            this.view.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
            return this;
        }

        /**
         * Method to set drawable on right side of button
         *
         * @param drawable: drawable that needs to be displayed on button
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setRightDrawableIcon(Drawable drawable) {
            this.view.setCompoundDrawables(null, null, drawable, null);
            return this;
        }

        /**
         * Method to set layout parameters on button
         *
         * @param params: layout parameters for button which will be height, width, margin so on
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setLayoutParam(ViewGroup.LayoutParams params) {
            this.view.setLayoutParams(params);
            return this;
        }

        /**
         * Method to set font style or font on text of button
         *
         * @param face: type face data which consist of default fonstyle or user defined fonts
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTypeFace(Typeface face) {
            this.view.setTypeface(face);
            return this;
        }

        /**
         * Method to set corner radius on button
         *
         * @param cornerRadius: radius value of how much curve user wants their button to be from corners
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setCornerRadius(int cornerRadius) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setCornerRadius(cornerRadius);
            return this;
        }

        /**
         * Method to set color of stroke of background if any on button
         *
         * @param stroke: parsed value of color
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setStrokeColor(int stroke) {
            if (drawable == null) {
                drawable = new GradientDrawable();
                drawable.setShape(GradientDrawable.RECTANGLE);
            }
            drawable.setStroke(1, stroke);
            return this;
        }

        /**
         * Method to set tag on button which will act as field/variable of request to sent to server
         *
         * @param tag: field name
         * @return ButtonBuilder: return instance of button builder
         */
        public ButtonBuilder setTag(String tag) { //Will act as parameters to be sent to server
            this.view.setTag(tag);
            return this;
        }

        /**
         * Method to return the button build after setting user defined attributes
         *
         * @return AppCompatButton: return instance of button
         */
        public AppCompatButton build() {
            if (drawable != null) {
                this.view.setBackgroundDrawable(drawable);
            }
            return this.view;
        }
    }

    /**
     * Builder class to build an Image view wth user defined attributes
     */
    public static class ImageViewBuilder {
        public Context context;
        private AppCompatImageView view;

        /**
         * Constructor for Image View builder
         *
         * @param context: take application context
         */
        public ImageViewBuilder(Context context) {
            this.context = context;
            this.view = new AppCompatImageView(context);
        }

        /**
         * Method to set layout parameters for image view
         *
         * @param params: layout parameter object which will set image view height, width, margins and so on
         * @return ImageViewBuilder: return instance of imageview builder
         */
        public ImageViewBuilder setLayoutParam(ViewGroup.LayoutParams params) {
            this.view.setLayoutParams(params);
            return this;
        }

        /**
         * Method to build the image view after setting all the user define attributes
         *
         * @return AppCompatImageView: return instance of Image view
         */
        public AppCompatImageView build() {
            return this.view;
        }
    }

}
