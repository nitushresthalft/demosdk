package com.singularkey.base.customviews

/**
 * Class that provides what value we take to set the overall app theme
 * like status bar color, action bar, title color and so on.
 * */
data class SingularKeyScreenViewData(
    val colorStatusBar: Int,
    val colorActionBar: Int,
    val colorActionBarTitle: Int,
)