package com.singularkey.base.ui.mfacontainer

import android.content.Context
import android.graphics.Typeface
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.BaseApplication
import com.singularkey.base.base.BaseViewModel
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.customviews.SingularKeyScreenViewData
import com.singularkey.base.data.model.*
import com.singularkey.base.repository.ApiListener
import com.singularkey.base.repository.BaseModuleRepository
import com.singularkey.base.ui.mfacontainer.screentemplateone.BASE64_FLAG

class SingularKeyViewModel(private var singularTone: SingularKeyTone) :
    BaseViewModel<SingularKeyViewNavigator>() {
    val repository = BaseModuleRepository.instance()

    var isVoice: Boolean? = false
    var configName: String? = ""
    var screenConfig: Screen3ConfigModel? = null
    var userviewIndex: Int = 0
    var isApiCall: Boolean = false
    var capabilityName: String = ""
    var eventName: String = ""
    var title: String = ""
    var onLoadEventName: String = ""
    var iconUrl: String = ""
    var bgColor: String = ""
    var onLoadEvent: LabelViewDetailModel? = null

    /**
     *Method to provide the view needed for FIDO Screen
     *
     * @param ctx: Application context
     * @param flow: Flow object
     * @param userViewPosition: user view position
     *
     * @return ArrayList<View>: List of views that need to shown in the FIDO Screen
     *
     **/
    fun getFIDOView(ctx: Context, flow: Flow, userViewPosition: Int): java.util.ArrayList<View> {
        var viewList = java.util.ArrayList<View>()
        if (flow.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flow.screen!!.properties!!.mfaList!!.value!!) {
                if (mfaVal.connectorId.equals("fido2Connector", true)) {
                    viewList =
                        getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                }
            }
        } else if (flow.screen!!.name!!.equals("FIDO2/WebAuthn", true)) {
            viewList = getViews(ctx,
                flow.screen!!.properties!!,
                flow.screen!!.userViews!!,
                userViewPosition)
        }

        return viewList
    }

    /**
     *Method to provide the view needed for Time based OTP Screen
     *
     * @param ctx: Application context
     * @param flow: Flow object
     * @param userViewPosition: user view position
     *
     * @return ArrayList<View>: List of views that need to shown in the FIDO Screen
     *
     **/
    fun getTOTPView(ctx: Context, flow: Flow, userViewPosition: Int): ArrayList<View> {
        var viewList = ArrayList<View>()
        if (flow.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flow.screen!!.properties!!.mfaList!!.value!!) {
                if (mfaVal.connectorId.equals("totpConnector", true)) {
                    bgColor = mfaVal.metadata!!.colors!!.canvas!!
                    viewList =
                        getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                }
            }
        } else if (flow.screen!!.name.equals("Time based OTP", true)) {
            bgColor = flow.screen!!.metadata!!.colors!!.canvas!!
            viewList = getViews(ctx,
                flow.screen!!.properties!!,
                flow.screen!!.userViews!!,
                userViewPosition)
        }

        return viewList
    }

    /**
     *Method to provide the view needed for SMS/Voice authentication Screen
     *
     * @param ctx: Application context
     * @param flow: Flow object
     * @param userViewPosition: user view position
     *
     * @return ArrayList<View>: List of views that need to shown in the FIDO Screen
     *
     **/
    fun getSmsView(ctx: Context, flow: Flow, userViewPosition: Int): ArrayList<View> {
        var viewList = ArrayList<View>()
        if (flow.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flow.screen!!.properties!!.mfaList!!.value!!) {
                if (!isVoice!!) {
                    if (mfaVal.connectorId.equals("smsConnector",
                            true)
                    ) {
                        bgColor = mfaVal.metadata!!.colors!!.canvas!!
                        viewList =
                            getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                    }
                } else {
                    if (mfaVal.connectorId.equals("VoiceConnector",
                            true)
                    ) {
                        bgColor = mfaVal.metadata!!.colors!!.canvas!!
                        viewList =
                            getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                    }
                }
            }
        } else {
            if (isVoice!!) {
                if (flow.screen!!.name.equals("Voice OTP",
                        true)
                ) {
                    bgColor = flow.screen!!.metadata!!.colors!!.canvas!!
                    viewList = getViews(ctx,
                        flow.screen!!.properties!!,
                        flow.screen!!.userViews!!,
                        userViewPosition)
                }
            } else {
                if (flow.screen!!.name.equals("SMS", true)) {
                    bgColor = flow.screen!!.metadata!!.colors!!.canvas!!
                    viewList = getViews(ctx,
                        flow.screen!!.properties!!,
                        flow.screen!!.userViews!!,
                        userViewPosition)
                }
            }
        }

        return viewList
    }

    /**
     *Method to provide the view needed for Jumio authentication Screen
     *
     * @param ctx: Application context
     * @param flow: Flow object
     * @param userViewPosition: user view position
     *
     * @return ArrayList<View>: List of views that need to shown in the FIDO Screen
     *
     **/
    fun getJumioView(ctx: Context, flow: Flow, userViewPosition: Int): ArrayList<View> {
        var viewList = ArrayList<View>()
        if (flow.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flow.screen!!.properties!!.mfaList!!.value!!) {
                if (mfaVal.connectorId.equals("jumioConnector",
                        true)
                ) {
                    bgColor = mfaVal.metadata!!.colors!!.canvas!!
                    viewList =
                        getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                }
            }
        } else {
            if (flow.screen!!.name.equals("Jumio",
                    true)
            ) {
                bgColor = flow.screen!!.metadata!!.colors!!.canvas!!
                viewList = getViews(ctx,
                    flow.screen!!.properties!!,
                    flow.screen!!.userViews!!,
                    userViewPosition)
            }
        }

        return viewList
    }

    /**
     *Method to provide the appropriate views (textview, edit text, button and so on)
     *  on the basis of field/information send by server in the form of flow model
     *
     * @param ctx: Application context
     * @param properties: Screen Properties which have explained
     * what view type need to be added in screen
     * @param userViews: User views list which has data for every screen that
     * needs to be shown to user to complete an authentication process
     * @param userViewPosition: user view position stating in which position/screen user is currently in
     *
     * @return ArrayList<View>: List of views that need to shown in the FIDO Screen
     *
     **/
    private fun getViews(
        ctx: Context,
        properties: ScreenPropertiesModel,
        userViews: ArrayList<UserView>,
        userViewPosition: Int,
    ): ArrayList<View> {
        var viewList = ArrayList<View>()
        configName = userViews[userViewPosition].screenConfigName

        if (configName.equals("screen0Config", true)) {
            screenConfig = properties.screen0Config
        } else if (configName.equals("screen1Config", true)) {
            screenConfig = properties.screen1Config
        } else if (configName.equals("screen2Config", true)) {
            screenConfig = properties.screen2Config
        } else if (configName.equals("screen3Config", true)) {
            screenConfig = properties.screen3Config
        } else if (configName.equals("screen4Config", true)) {
            screenConfig = properties.screen4Config
        } else if (configName.equals("screen5Config", true)) {
            screenConfig = properties.screen5Config
        }

        if (screenConfig != null) {
            if (screenConfig!!.properties!!.nextEvent != null) {
                val constructType = screenConfig!!.properties!!.nextEvent!!.constructType!!
                if (constructType.equals("skEvent", true)) {
                    val eventType = screenConfig!!.properties!!.nextEvent!!.eventType!!
                    if (eventType.equals("post", true)) {
                        eventName = screenConfig!!.properties!!.nextEvent!!.eventName!!
                        isApiCall = true
                    } else if (eventType.equals("nextUserView", true)) {
                        userviewIndex =
                            screenConfig!!.properties!!.nextEvent!!.nextUserViewIndex!!
                    }
                }
            }
            if (screenConfig!!.properties!!.onLoadEvent != null) {
                val constructType = screenConfig!!.properties!!.onLoadEvent!!.constructType!!
                if (constructType.equals("skEvent", true)) {
                    val eventType = screenConfig!!.properties!!.onLoadEvent!!.eventType!!
                    if (eventType.equals("post", true)) {
                        onLoadEventName = screenConfig!!.properties!!.onLoadEvent!!.eventName!!
                        onLoadEvent = screenConfig!!.properties!!.onLoadEvent!!
                        isApiCall = true
                        //TODO Perfrom Task
                    }
                }
            }
        }
        if (userViews[userViewPosition].items!!.isEmpty()) {
            title = properties.title!!.value!!

            var textViewBuilder = CustomView.TextViewBuilder(ctx)
                .setText(title)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTypeFace(Typeface.DEFAULT_BOLD)
                .setTextSize(16f)
                .setGravity(Gravity.CENTER_HORIZONTAL)

            viewList.add(textViewBuilder.build())

            var descriptionTextBuilder = CustomView.TextViewBuilder(ctx)
                .setText(properties.description!!.value)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTypeFace(Typeface.DEFAULT_BOLD)
                .setTextSize(16f)
                .setGravity(Gravity.CENTER_HORIZONTAL)

            viewList.add(descriptionTextBuilder.build())
        } else {
            for (items in userViews[userViewPosition].items!!) {
                if (items.propertyName.equals("Title", true)) {
                    title = properties.title!!.value!!
                    var textViewBuilder = CustomView.TextViewBuilder(ctx)
                        .setText(title)
                        .setLayoutParam(LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        ))
                        .setTypeFace(Typeface.DEFAULT_BOLD)
                        .setTextSize(16f)
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                    //viewList.add(tvTitle)
                }

                if (items.propertyName.equals("iconUrlPng", true)) {

                    val layoutParams = LinearLayout.LayoutParams(
                        60,
                        60
                    )
                    layoutParams.gravity = Gravity.CENTER_HORIZONTAL

                    var imageViewBuilder = CustomView.ImageViewBuilder(ctx)
                        .setLayoutParam(layoutParams)

                    iconUrl = properties.iconUrlPng!!.value!!
                    Glide.with(ctx).load(properties.iconUrlPng!!.value)
                        .into(imageViewBuilder.build())
                    viewList.add(imageViewBuilder.build())
                }
                if (items.propertyName.equals("description", true)) {
                    var layoutParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    layoutParam.topMargin = 10
                    var titleTextBuilder = CustomView.TextViewBuilder(ctx)
                        .setLayoutParam(layoutParam)
                        .setText(properties.description!!.value)
                        .setTextSize(16f)
                        .setGravity(Gravity.CENTER_HORIZONTAL)


                    viewList.add(titleTextBuilder.build())
                }

                if (items.propertyName.equals("authDescription", true)) {
                    var layoutParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    var authDescTextBuilder = CustomView.TextViewBuilder(ctx)
                        .setLayoutParam(layoutParam)
                        .setText(properties.authDescription!!.value)
                        .setTextSize(12f)
                        .setPadding(16, 16, 16, 16)
                        .setGravity(Gravity.CENTER_HORIZONTAL)

                    viewList.add(authDescTextBuilder.build())
                }


            }

        }
        return viewList
    }

    /**
     *Method is called for start to registration of user
     * for Time base OTP authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start registration process
     *
     *
     **/
    fun initTOTPRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.challenge!!)
                        if (onLoadEventName.isNullOrEmpty())
                            userviewIndex += 1
                        navigator.onRegistrationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called for start to login process of user
     * for Time base OTP authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start authentication/login process
     *
     *
     **/
    fun initTOTPAuthInitiation(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.challenge!!)
                        //userviewIndex += 1
                        navigator.onAuthInitiationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /* fun convertBase64ToImage(encodedString: String): Bitmap {
         var pureBase64Encoded = encodedString.substring(encodedString.indexOf(",") + 1)
         val decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT)
         val decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
         return decodedBitmap
     }*/

    /**
     *Method is called for complete the registration of user
     * for Time base OTP authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration complete request containing all the data
     * needed to complete authentication process
     *
     *
     **/
    fun completeTOTPAuthRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.onAuthenticationComplete(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    fun getViewData() = SingularKeyScreenViewData(singularTone.colorStatusBar,
        singularTone.colorActionBar,
        singularTone.colorActionBarTitle)

    /**
     *Method is called to start the flow for singular key authentication
     *
     * @param token: Authentication token
     * @param username: user name entered by user
     * @param flow_id: flow id received from server
     * @param company_id: Id given to the company by Singular Key
     * @param policy_id: policy id received from server
     *
     *
     **/
    fun startFlowWithId(
        company_id: String,
        policy_id: String?,
        flow_id: String?,
        token: String,
        username: String,
    ) {
        Log.e("TAG", policy_id + ":" + flow_id)
        getCompositeDisposable().add(
            repository.startFlow(
                company_id,
                policy_id,
                flow_id,
                token,
                username,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.onFlowSuccess(successData as Flow)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                })
        )
    }

    /**
     * Method is called for start to registration of user
     * for SMS authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start registration process
     *
     *
     **/
    fun initSmsRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.challenge!!)
                        if (onLoadEventName.isEmpty())
                            userviewIndex += 1
                        navigator.onRegistrationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to start authentication/login process of user
     * for SMS authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start registration process
     *
     *
     **/
    fun initSmsAuthInitiation(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.challenge!!)
                        navigator.onAuthInitiationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }


    /**
     *Method is called to complete authentication process of user
     * using Singular Key
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration complete request containing all the data
     * needed to complete authentication process
     *
     *
     **/
    fun completeSmsAuthRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.onAuthenticationComplete(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to initialize registration process of user
     * for FIDO
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start authentication process
     *
     *
     **/
    fun initFIDORegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.initiateRegistrationResponse!!.challenge!!)
                        var authenticatorAttachement = ""
                        if (response.initiateRegistrationResponse!!.authenticatorSelection != null)
                            authenticatorAttachement =
                                response.initiateRegistrationResponse!!.authenticatorSelection!!.authenticatorAttachment!!

                        val attestation = response.initiateRegistrationResponse!!.attestation
                        Log.d("LOG_TAG", attestation!!)
                        var attestationPreference: AttestationConveyancePreference =
                            AttestationConveyancePreference.NONE
                        if (attestation == "direct") {
                            attestationPreference = AttestationConveyancePreference.DIRECT
                        } else if (attestation == "indirect") {
                            attestationPreference = AttestationConveyancePreference.INDIRECT
                        } else if (attestation == "none") {
                            attestationPreference = AttestationConveyancePreference.NONE
                        }
                        val challenge = Base64.decode(
                            response.initiateRegistrationResponse!!.challenge!!,
                            BASE64_FLAG
                        )

                        navigator.fido2AndroidRegister(
                            response.initiateRegistrationResponse!!.rp!!.name!!,
                            challenge,
                            response.initiateRegistrationResponse!!.user!!.id!!,
                            response.initiateRegistrationResponse!!.user!!.name,
                            authenticatorAttachement,
                            attestationPreference
                        )
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to complete authentication process of user
     * using Singular Key with FIDO
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration complete request containing all the data
     * needed to complete authentication process
     *
     *
     **/
    fun completeFIDORegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.onAuthenticationComplete(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to start authentication/login process of user
     * in Singular Key with FIDO
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: authentication init request containing all the data
     * needed to start authentication process
     *
     *
     **/
    fun initFIDOAuthentication(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.initiateAuthenticationResponse!!.challenge!!)
                        val challenge = Base64.decode(
                            response.initiateAuthenticationResponse!!.challenge!!,
                            BASE64_FLAG
                        )

                        navigator.fido2Auth(
                            response.initiateAuthenticationResponse!!.allowCredentials!!,
                            challenge,
                            response.initiateAuthenticationResponse!!.rpId!!
                        )
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to complete authentication process of user
     * using Singular Key with FIDO
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration complete request containing all the data
     * needed to complete authentication process
     *
     *
     **/
    fun completeFIDOAuthentication(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.onAuthenticationComplete(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to start registration process of user
     * using Singular Key with JUMIO for authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration init request containing all the data
     * needed to start registration process
     *
     *
     **/
    fun initJumioRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        /*BaseApplication.instance()
                            .saveChallenge(response.challenge!!)*/
                        if (onLoadEventName.isEmpty())
                            userviewIndex += 1
                        navigator.onRegistrationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to start authentication/login process of user
     * using Singular Key with JUMIO for authentication
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: authentication init request containing all the data
     * needed to start authentication process
     *
     *
     **/
    fun initJumioAuthInitiation(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {
        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        /*BaseApplication.instance()
                            .saveChallenge(response.challenge!!)*/
                        navigator.onAuthInitiationSuccess(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    /**
     *Method is called to complete authentication process of user
     * using Singular Key with JUMIO
     *
     * @param token: Authentication token
     * @param interactionId: Interaction id received from server
     * @param connectionId: Connection id received from server
     * @param companyId: Id given to the company by Singular Key
     * @param capabilityName: Capability name received from server
     * @param request: registration complete request containing all the data
     * needed to complete registration process
     *
     *
     **/
    fun completeJumioAuthRegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationSuccess =
                            successData as AuthenticationRegistrationSuccess

                        if (onLoadEventName.isEmpty())
                            userviewIndex += 1
                        navigator.onAuthenticationComplete(response)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }
}