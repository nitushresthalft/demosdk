package com.singularkey.base.ui.mfacontainer.TOTPScreen.fragments

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.SubScreensFragmentBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator
import com.singularkey.base.ui.mfacontainer.TOTPScreen.TOTPScreenMainFragment
import com.singularkey.base.utils.SingularKeyConstantUtils

class TOTPScreenTwoFragment(
    var mViewModel: SingularKeyViewModel,
    var response: AuthenticationRegistrationBaseResponse,
) :
    BaseFragment<SubScreensFragmentBinding>(), SingularKeyViewNavigator {
    var flowData: Flow? = null
    private lateinit var mBinding: SubScreensFragmentBinding

    override fun getLayoutId() = R.layout.sub_screens_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as SubScreensFragmentBinding
        mViewModel.navigator = this
        flowData = BaseApplication.instance().flowData
        setupView()
    }

    fun setupView() {
        var viewList = mViewModel.getTOTPView(context!!, flowData!!, mViewModel.userviewIndex)
        for (view in viewList) {
            mBinding.llViewContainer.addView(view)
        }

        if (mViewModel.screenConfig != null) {
            if (mViewModel.screenConfig!!.properties!!.qr != null) {

                var layoutParam = LinearLayout.LayoutParams(
                    500,
                    500
                )
                layoutParam.setMargins(20, 20, 20, 20)
                layoutParam.gravity = Gravity.CENTER_HORIZONTAL
                var imageViewBuilder = CustomView.ImageViewBuilder(context!!)
                    .setLayoutParam(layoutParam)

                Glide.with(context!!).load(SingularKeyConstantUtils.convertBase64ToImage(response.qr!!))
                    .into(imageViewBuilder.build())

                mBinding.llViewContainer.addView(imageViewBuilder.build())
            }

            var linearLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            linearLayoutParam.setMargins(20, 20, 20, 20)
            var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                .setLayoutParam(linearLayoutParam)
                .setOrientation(LinearLayout.HORIZONTAL)
                .setPadding(16, 16, 16, 16)
                .setGravity(Gravity.CENTER)
                .setCornerRadius(16)
                .setBackground(Color.parseColor(mViewModel.bgColor))
            var linearLayout = linearLayoutBuilder.build()

            var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
            imageLayoutParams.setMargins(10, 10, 10, 10)
            imageLayoutParams.gravity = Gravity.CENTER_VERTICAL
            var imageViewBuilder = CustomView.ImageViewBuilder(context!!)
                .setLayoutParam(imageLayoutParams)

            Glide.with(context!!).load(mViewModel.iconUrl).into(imageViewBuilder.build())
            linearLayout.addView(imageViewBuilder.build())

            var buttonViewBuilder = CustomView.TextViewBuilder(context!!)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setText(mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!)
                .setTextSize(12f)
                .setTextColor(resources.getColor(R.color.white))
                .setBackground(resources.getColor(android.R.color.transparent))

            linearLayout.addView(buttonViewBuilder.build())
            linearLayout.setOnClickListener {
                (parentFragment as TOTPScreenMainFragment).gotoThirdFragment()
            }

            mBinding.llViewContainer.addView(linearLayout)
        }
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {

    }

    override fun showMessage(msg: String) {

    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {

    }

    override fun onError(msg: BaseError) {
        loadingObservable.value = false
        (parentFragment as TOTPScreenMainFragment).onFlowError(msg)
    }
}