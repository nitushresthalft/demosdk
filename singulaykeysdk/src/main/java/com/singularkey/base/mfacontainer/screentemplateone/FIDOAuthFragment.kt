package com.singularkey.base.ui.mfacontainer.screentemplateone

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.Color
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.gms.fido.Fido
import com.google.android.gms.fido.fido2.api.common.*
import com.gox.base.extensions.provideViewModel
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.ScreenTemplateOneLayoutBinding
import com.singularkey.base.repository.SingularKeyApiConstants
import com.singularkey.base.ui.mfacontainer.SingularKeyActivity
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator
import org.json.JSONObject

/**
 * Main Fragment for FIDO authentication
 */
class FIDOAuthFragment : BaseFragment<ScreenTemplateOneLayoutBinding>(),
    SingularKeyViewNavigator {

    companion object {
        public const val REQUEST_CODE_REGISTER = 1
        public const val REQUEST_CODE_SIGN = 2
        private const val LOG_TAG = "Log tag"
    }


    private lateinit var mBinding: ScreenTemplateOneLayoutBinding
    private lateinit var mViewModel: SingularKeyViewModel
    private lateinit var flowData: Flow
    private lateinit var userName: String
    private lateinit var token: String
    private lateinit var bgColor: String
    private lateinit var postProcessEvent: ProcessEventModel
    private lateinit var mActivity: SingularKeyActivity

    override fun getLayoutId() = R.layout.screen_template_one_layout

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as ScreenTemplateOneLayoutBinding
        mViewModel = provideViewModel { SingularKeyViewModel(SingularKeyTone) }
        mViewModel.navigator = this
        flowData = BaseApplication.instance().flowData!!
        userName = BaseApplication.instance().username!!
        token = BaseApplication.instance().authToken!!
        setupView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SingularKeyActivity)
            mActivity = context as SingularKeyActivity
    }

    fun setupView() {
        var viewList = mViewModel.getFIDOView(context!!, flowData, 0)
        var title: String = ""
        mActivity.setTitle(mViewModel.title)
        for (view in viewList) {
            mBinding.llScreenTemplateContainer.addView(view)
        }
        if (flowData.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flowData.screen!!.properties!!.mfaList!!.value!!) {
                if (mfaVal.connectorId.equals("fido2Connector", true)) {
                    title = mfaVal.name!!
                    bgColor = mfaVal.metadata!!.colors!!.canvas!!
                    viewCreate()
                }
            }
        } else if (flowData.screen!!.name!!.equals("FIDO2/WebAuthn", true)) {
            title = flowData.screen!!.name!!
            bgColor = flowData.screen!!.metadata!!.colors!!.canvas!!
            viewCreate()
        }
        mActivity.setTitle(title)
    }


    private fun viewCreate() {
        if (mViewModel.screenConfig!!.properties!!.nextEvent != null) {
            var nextEvent = mViewModel.screenConfig!!.properties!!.nextEvent!!
            if (nextEvent.eventName.equals("registerInitiate")) {
                postProcessEvent = nextEvent.postProcess!!
                var linearLayoutParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                linearLayoutParam.setMargins(20, 20, 20, 20)
                var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                    .setLayoutParam(linearLayoutParam)
                    .setOrientation(LinearLayout.HORIZONTAL)
                    .setPadding(16, 16, 16, 16)
                    .setGravity(Gravity.CENTER)
                    .setCornerRadius(16)
                    .setBackground(Color.parseColor(bgColor))
                var linearLayout = linearLayoutBuilder.build()

                var imageIcon = ImageView(context)
                var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
                imageLayoutParams.setMargins(10, 10, 10, 10)
                imageLayoutParams.gravity = Gravity.CENTER_VERTICAL
                imageIcon.layoutParams = imageLayoutParams
                Glide.with(this).load(mViewModel.iconUrl).into(imageIcon)
                linearLayout.addView(imageIcon)

                val btnValue = TextView(context)
                btnValue.text =
                    mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!

                btnValue.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                btnValue.textSize = 12f
                btnValue.setTextColor(resources.getColor(R.color.white))
                btnValue.setBackgroundColor(resources.getColor(android.R.color.transparent))
                linearLayout.addView(btnValue)

                var request = RegistrationInitiationRequest()
                request.capabilityName = flowData.capabilityName
                request.connectionId = BaseApplication.instance().connectionId
                request.eventName = nextEvent.eventName

                linearLayout.setOnClickListener {
                    loadingObservable.value = true
                    mViewModel.initFIDORegistration(
                        BaseApplication.instance().authToken!!,
                        flowData.interactionId!!,
                        flowData.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData.capabilityName!!,
                        request
                    )
                }
                mBinding.llScreenTemplateContainer.addView(linearLayout)
            }
        } else if (mViewModel.screenConfig!!.properties!!.onLoadEvent != null) {
            var nextEvent = mViewModel.screenConfig!!.properties!!.onLoadEvent!!
            if (nextEvent.eventName.equals("authInitiate", true)) {
                postProcessEvent = nextEvent.postProcess!!

                var linearLayoutParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
                linearLayoutParam.setMargins(20, 20, 20, 20)
                var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                    .setLayoutParam(linearLayoutParam)
                    .setOrientation(LinearLayout.HORIZONTAL)
                    .setPadding(16, 16, 16, 16)
                    .setGravity(Gravity.CENTER)
                    .setCornerRadius(16)
                    .setBackground(Color.parseColor(bgColor))
                var linearLayout = linearLayoutBuilder.build()


                var imageIcon = ImageView(context)
                var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
                imageLayoutParams.setMargins(10, 10, 10, 10)
                imageLayoutParams.gravity = Gravity.CENTER_VERTICAL
                imageIcon.layoutParams = imageLayoutParams
                Glide.with(this).load(mViewModel.iconUrl).into(imageIcon)
                linearLayout.addView(imageIcon)
                val btnValue = TextView(context)
                btnValue.text =
                    mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!

                btnValue.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                btnValue.textSize = 12f
                btnValue.setTextColor(resources.getColor(R.color.white))
                btnValue.setBackgroundColor(resources.getColor(android.R.color.transparent))
                linearLayout.addView(btnValue)

                var request = RegistrationInitiationRequest()
                request.capabilityName = flowData.capabilityName
                request.connectionId = BaseApplication.instance().connectionId
                request.eventName = nextEvent.eventName

                linearLayout.setOnClickListener {
                    loadingObservable.value = true
                    mViewModel.initFIDOAuthentication(
                        BaseApplication.instance().authToken!!,
                        flowData.interactionId!!,
                        flowData.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData.capabilityName!!,
                        request
                    )
                }

                mBinding.llScreenTemplateContainer.addView(linearLayout)
            }
        }
    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {
        try {
            val list = mutableListOf<PublicKeyCredentialDescriptor>()
            for (i in allowCredentials.indices) {
                val item = allowCredentials[i]
                list.add(
                    PublicKeyCredentialDescriptor(
                        PublicKeyCredentialType.PUBLIC_KEY.toString(),
                        Base64.decode(item.id, BASE64_FLAG),
                        /* transports */ null
                    )
                )
            }

            val options = PublicKeyCredentialRequestOptions.Builder()
                .setRpId(rpid)
                .setAllowList(list)
                .setChallenge(challenge)
                .build()

            val fido2ApiClient = Fido.getFido2ApiClient(context)
            val fido2PendingIntentTask = fido2ApiClient.getSignIntent(options)
            fido2PendingIntentTask.addOnSuccessListener { fido2PendingIntent ->
                if (fido2PendingIntent.hasPendingIntent()) {
                    try {
                        Log.d(LOG_TAG, "launching Fido2 Pending Intent")
                        fido2PendingIntent.launchPendingIntent(mActivity, REQUEST_CODE_SIGN)
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun showMessage(msg: String) {

    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {
        loadingObservable.value = false
        mActivity.onSuccessfulRegistration(response)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(LOG_TAG, "onActivityResult - requestCode: $requestCode, resultCode: $resultCode")
        when (resultCode) {
            Activity.RESULT_OK -> {
                data?.let {
                    if (it.hasExtra(Fido.FIDO2_KEY_ERROR_EXTRA)) {

                        val errorExtra = data.getByteArrayExtra(Fido.FIDO2_KEY_ERROR_EXTRA)
                        val authenticatorErrorResponse =
                            AuthenticatorErrorResponse.deserializeFromBytes(errorExtra!!)
                        val errorName = authenticatorErrorResponse.errorCode.name
                        val errorMessage = authenticatorErrorResponse.errorMessage

                        Log.e(LOG_TAG, "errorCode.name: $errorName")
                        Log.e(LOG_TAG, "errorMessage: $errorMessage")

                        showError("An Error Occurred\n\nError Name:\n$errorName\n\nError Message:\n$errorMessage")
                        loadingObservable.value = false
                    } else if (it.hasExtra(Fido.FIDO2_KEY_RESPONSE_EXTRA)) {
                        val fido2Response =
                            data.getByteArrayExtra(Fido.FIDO2_KEY_RESPONSE_EXTRA)
                        when (requestCode) {
                            REQUEST_CODE_REGISTER -> {
                                fido2RegistrationComplete(fido2Response!!)
                            }
                            REQUEST_CODE_SIGN -> {
                                fido2AuthComplete(fido2Response!!)
                            }
                        }
                    }
                }
            }
            Activity.RESULT_CANCELED -> {
                loadingObservable.value = false
                val result = "Operation is cancelled"
                showError(result)
                Log.d(LOG_TAG, result)
            }
            else -> {
                val result = "Operation failed, with resultCode: $resultCode"
                showError(result)
                Log.e(LOG_TAG, result)
            }
        }
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {
        try {
            val options = PublicKeyCredentialCreationOptions.Builder()
                .setAttestationConveyancePreference(attestationPreference)
                .setRp(PublicKeyCredentialRpEntity(SingularKeyApiConstants.RPID, rpname, null))
                .setUser(
                    PublicKeyCredentialUserEntity(
                        userId.toByteArray(),
                        userId,
                        null,
                        userName
                    )
                )
                .setChallenge(challenge)
                .setParameters(
                    listOf(
                        PublicKeyCredentialParameters(
                            PublicKeyCredentialType.PUBLIC_KEY.toString(),
                            EC2Algorithm.ES256.algoValue
                        )
                    )
                )

            if (authenticatorAttachment != "") {
                val builder = AuthenticatorSelectionCriteria.Builder()
                builder.setAttachment(Attachment.fromString("platform"))
                options.setAuthenticatorSelection(builder.build())
            }

            val fido2ApiClient = Fido.getFido2ApiClient(context)
            val fido2PendingIntentTask = fido2ApiClient.getRegisterIntent(options.build())
            fido2PendingIntentTask.addOnSuccessListener { fido2PendingIntent ->
                if (fido2PendingIntent.hasPendingIntent()) {
                    try {
                        Log.d("LOG_TAG", "launching Fido2 Pending Intent")
                        fido2PendingIntent.launchPendingIntent(
                            mActivity,
                            REQUEST_CODE_REGISTER
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fido2RegistrationComplete(fido2Response: ByteArray) {
        var request = SingularKeyRegistrationCompleteRequest()
        request.capabilityName = flowData.capabilityName
        request.connectionId = BaseApplication.instance().connectionId
        request.eventName = postProcessEvent.nextEvent!!.eventName
        val attestationResponse =
            AuthenticatorAttestationResponse.deserializeFromBytes(
                fido2Response
            )
        val credId = Base64.encodeToString(
            attestationResponse.keyHandle,
            BASE64_FLAG
        )

        val clientDataJson = Base64.encodeToString(
            attestationResponse.clientDataJSON,
            BASE64_FLAG
        )
        val attestationObjectBase64 = Base64.encodeToString(
            attestationResponse.attestationObject,
            Base64.DEFAULT
        )

        val webAuthnResponse = WebAuthnResponseData()
        val response = WebauthResponseModel()
        response.attestationObject = attestationObjectBase64
        response.clientDataJSON = clientDataJson

        webAuthnResponse.type = "public-key"
        webAuthnResponse.id = credId
        webAuthnResponse.rawId = credId
        webAuthnResponse.response = response

        request.id = flowData.id
        var mainRequest = RegistrationCompleteRequestModel()
        mainRequest.webAuthnResponse = webAuthnResponse
        mainRequest.challenge = BaseApplication.instance().challenge
        request.parameters = mainRequest

        mViewModel.completeFIDORegistration(
            BaseApplication.instance().authToken!!,
            flowData.interactionId!!,
            flowData.companyId!!,
            BaseApplication.instance().connectionId!!,
            flowData.capabilityName!!,
            request
        )
    }

    private fun fido2AuthComplete(fido2Response: ByteArray) {

        val assertionResponse = AuthenticatorAssertionResponse.deserializeFromBytes(fido2Response)
        val credId = Base64.encodeToString(assertionResponse.keyHandle, BASE64_FLAG)
        val signature = Base64.encodeToString(assertionResponse.signature, BASE64_FLAG)
        val authenticatorData =
            Base64.encodeToString(assertionResponse.authenticatorData, BASE64_FLAG)
        val clientDataJson = Base64.encodeToString(assertionResponse.clientDataJSON, BASE64_FLAG)


        val response = WebauthResponseModel()
        response.clientDataJSON = clientDataJson
        response.signature = signature
        response.userHandle = ""
        response.authenticatorData = authenticatorData

        val webAuthnResponse = WebAuthnResponseData()
        webAuthnResponse.type = "public-key"
        webAuthnResponse.id = credId
        webAuthnResponse.rawId = credId
        webAuthnResponse.getClientExtensionResults = JSONObject()
        webAuthnResponse.response = response

        var request = SingularKeyRegistrationCompleteRequest()
        request.capabilityName = flowData.capabilityName
        request.connectionId = BaseApplication.instance().connectionId
        request.eventName = postProcessEvent.nextEvent!!.eventName
        request.id = flowData.id
        var mainRequest = RegistrationCompleteRequestModel()
        mainRequest.webAuthnResponse = webAuthnResponse
        mainRequest.challenge = BaseApplication.instance().challenge
        request.parameters = mainRequest

        mViewModel.completeFIDOAuthentication(
            BaseApplication.instance().authToken!!,
            flowData.interactionId!!,
            flowData.companyId!!,
            BaseApplication.instance().connectionId!!,
            flowData.capabilityName!!,
            request
        )

    }

    override fun onError(error: BaseError) {
        loadingObservable.value = false
        showError(error.message!!)
        mActivity.onRegistrationError(error)
    }

}