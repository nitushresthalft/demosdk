package com.singularkey.base.ui.mfacontainer.TOTPScreen

import android.content.Context
import android.view.View
import androidx.databinding.ViewDataBinding
import com.gox.base.extensions.provideViewModel
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.data.model.AuthenticationRegistrationBaseResponse
import com.singularkey.base.data.model.AuthenticationRegistrationSuccess
import com.singularkey.base.data.model.BaseError
import com.singularkey.base.data.model.Flow
import com.singularkey.base.databinding.MainActivityBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyActivity
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.TOTPScreen.fragments.TOTPScreenOneFragment
import com.singularkey.base.ui.mfacontainer.TOTPScreen.fragments.TOTPScreenThreeFragment
import com.singularkey.base.ui.mfacontainer.TOTPScreen.fragments.TOTPScreenTwoFragment

/**
 * Main fragment for Time base OTP authentication
 *
 * */
class TOTPScreenMainFragment : BaseFragment<MainActivityBinding>() {

    private lateinit var mBinding: MainActivityBinding
    private lateinit var mViewModel: SingularKeyViewModel
    private lateinit var flowData: Flow
    private lateinit var userName: String
    private lateinit var token: String
    private lateinit var mActivity: SingularKeyActivity

    override fun getLayoutId() = R.layout.main_activity

    fun setupView() {
        if (mViewModel.userviewIndex == 0) {
            childFragmentManager.beginTransaction()
                .add(R.id.fragment_container, TOTPScreenOneFragment(mViewModel))
                .commitAllowingStateLoss()
        }
    }

    fun gotoSecondFragment(response: AuthenticationRegistrationBaseResponse) {
        childFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, TOTPScreenTwoFragment(mViewModel, response))
            .commitAllowingStateLoss()
    }

    fun gotoThirdFragment() {
        childFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, TOTPScreenThreeFragment(mViewModel))
            .commitAllowingStateLoss()
    }

    fun onSuccessfulRegistration(response: AuthenticationRegistrationSuccess) {
        loadingObservable.value = false
        mActivity.onSuccessfulRegistration(response)
    }

    fun onFlowError(response: BaseError) {
        loadingObservable.value = false
        mActivity.onRegistrationError(response)
    }

    fun setTitle(msg: String) {
        mActivity.setTitle(msg)
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as MainActivityBinding
        mViewModel = provideViewModel { SingularKeyViewModel(SingularKeyTone) }

        flowData = BaseApplication.instance().flowData!!
        userName = BaseApplication.instance().username!!
        token = BaseApplication.instance().authToken!!
        setupView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SingularKeyActivity) {
            mActivity = context as SingularKeyActivity
        }
    }
}