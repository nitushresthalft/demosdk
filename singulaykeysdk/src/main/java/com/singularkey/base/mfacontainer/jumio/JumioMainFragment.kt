package com.singularkey.base.ui.mfacontainer.jumio

import android.content.Context
import android.view.View
import androidx.databinding.ViewDataBinding
import com.gox.base.extensions.provideViewModel
import com.jumio.MobileSDK
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.data.model.AuthenticationRegistrationSuccess
import com.singularkey.base.data.model.BaseError
import com.singularkey.base.data.model.Flow
import com.singularkey.base.databinding.MainActivityBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyActivity
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel

/**
 * Main fragment for JUMIO authentication
 *
 * */
class JumioMainFragment : BaseFragment<MainActivityBinding>() {

    private lateinit var mBinding: MainActivityBinding
    private lateinit var mViewModel: SingularKeyViewModel
    private lateinit var flowData: Flow
    private lateinit var userName: String
    private lateinit var token: String
    lateinit var mActivity: SingularKeyActivity

    override fun getLayoutId() = R.layout.main_activity

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as MainActivityBinding
        mViewModel = provideViewModel { SingularKeyViewModel(SingularKeyTone) }
        flowData = BaseApplication.instance().flowData!!
        userName = BaseApplication.instance().username!!
        token = BaseApplication.instance().authToken!!
        setupView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SingularKeyActivity)
            mActivity = context as SingularKeyActivity
    }

    fun setupView() {
        if (mViewModel.userviewIndex == 0) {
            childFragmentManager.beginTransaction()
                .add(R.id.fragment_container, JumioFragmentScreenOne(mViewModel))
                .commitAllowingStateLoss()
        }
    }

    /**
     * Check and request missing permissions for the SDK.
     *
     * @param requestCode the request code for the SDK
     */
    fun checkPermissions(requestCode: Int): Boolean {
        return if (!MobileSDK.hasAllRequiredPermissions(context)) { //Acquire missing permissions.
            val mp = MobileSDK.getMissingPermissions(context)
            requestPermissions(mp, requestCode)
            //The result is received in onRequestPermissionsResult.
            false
        } else {
            true
        }
    }

    fun setTitle(msg: String) {
        mActivity.setTitle(msg)
    }

    fun onSuccessfulRegistration(response: AuthenticationRegistrationSuccess) {
        loadingObservable.value = false
        mActivity.onSuccessfulRegistration(response)
        /*  var intent = Intent()
          intent.putExtra(ScreenTemplateOne.FIDO_RESPONSE_DATA, Gson().toJson(response))
          setResult(AppCompatActivity.RESULT_OK, intent)
          finish()*/
    }

    fun onFlowError(error: BaseError) {
        loadingObservable.value = false
        mActivity.onRegistrationError(error)
    }
}