package com.singularkey.base.ui.mfacontainer.smsscreen.fragments

import android.view.View
import androidx.databinding.ViewDataBinding
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.SubScreensFragmentBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator

class SmsScreenThreeFragment(
    var mViewModel: SingularKeyViewModel,
) :
    BaseFragment<SubScreensFragmentBinding>(), SingularKeyViewNavigator {

    private lateinit var mBinding: SubScreensFragmentBinding

    override fun getLayoutId() = R.layout.sub_screens_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as SubScreensFragmentBinding
        mViewModel.navigator = this
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {

    }

    override fun showMessage(msg: String) {
    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {

    }

    override fun onError(error: BaseError) {
        showError(error.message!!)
    }
}