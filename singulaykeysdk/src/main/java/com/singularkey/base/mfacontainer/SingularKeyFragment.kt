package com.singularkey.base.ui.mfacontainer

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.Flow
import com.singularkey.base.databinding.MfaListFragmentBinding

/**
 * Fragment to show the list of options for authentication process
 * */
class SingularKeyFragment(var mfaViewModel: SingularKeyViewModel, var flowData: Flow) :
    BaseFragment<MfaListFragmentBinding>() {
    private lateinit var mBinding: MfaListFragmentBinding
    private lateinit var mActivity: SingularKeyActivity

    override fun getLayoutId() = R.layout.mfa_list_fragment
    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as MfaListFragmentBinding
        setupView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SingularKeyActivity) {
            mActivity = context as SingularKeyActivity
        }
    }

    fun setupView() {
        mActivity.setTitle(flowData.screen!!.properties!!.title!!.value!!)
        var textViewBuilder = CustomView.TextViewBuilder(context!!)
            .setLayoutParam(LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            ))
            .setText(flowData.screen!!.properties!!.bodyText!!.value!!)
            .setTextColor(Color.LTGRAY)
            .setTextSize(12f)
        mBinding.llContainer.addView(textViewBuilder.build())
        for (mfaval in flowData.screen!!.properties!!.mfaList!!.value!!) {

            var linearLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            linearLayoutParam.setMargins(20, 20, 20, 20)
            var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                .setLayoutParam(linearLayoutParam)
                .setOrientation(LinearLayout.HORIZONTAL)
                .setPadding(16, 16, 16, 16)

            var linearLayout = linearLayoutBuilder.build()

            var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
            imageLayoutParams.setMargins(10, 10, 10, 10)
            imageLayoutParams.gravity = Gravity.CENTER_VERTICAL

            var imageViewBuilder = CustomView.ImageViewBuilder(context!!)
                .setLayoutParam(imageLayoutParams)

            Glide.with(this).load(mfaval.properties!!.iconUrlPng!!.value)
                .into(imageViewBuilder.build())
            linearLayout.addView(imageViewBuilder.build())

            var childLinearLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            childLinearLayoutParam.setMargins(20, 10, 10, 10)
            var childlinearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                .setLayoutParam(childLinearLayoutParam)
                .setGravity(Gravity.LEFT)
                .setOrientation(LinearLayout.VERTICAL)
            var childlinearLayout = childlinearLayoutBuilder.build()

            var textBtnBuilder = CustomView.TextViewBuilder(context!!)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setText(mfaval.name!!)
                .setTextSize(16f)
                .setTypeFace(Typeface.DEFAULT_BOLD)

            childlinearLayout.addView(textBtnBuilder.build())

            val decTextBuilder = CustomView.TextViewBuilder(context!!)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setText(mfaval.properties!!.description!!.value!!)
                .setTextSize(12f)

            childlinearLayout.addView(decTextBuilder.build())
            linearLayout.addView(childlinearLayout)
            linearLayout.setOnClickListener {
                Toast.makeText(context, mfaval.name!! + " clicked", Toast.LENGTH_SHORT).show()
                if (mfaval.connectorId!!.equals("fido2Connector", true)) {
                    if (mfaval.connectionId != null) {
                        BaseApplication.instance().setConnectionid(mfaval.connectionId!!)
                    } else {
                        BaseApplication.instance().setConnectionid(flowData.connectionId!!)
                    }
                    mActivity.gotoFIDOAuthScreen()

                } else if (mfaval.connectorId!!.equals("totpConnector", true)) {
                    if (mfaval.connectionId != null) {
                        BaseApplication.instance().setConnectionid(mfaval.connectionId!!)
                    } else {
                        BaseApplication.instance().setConnectionid(flowData.connectionId!!)
                    }
                    mActivity.gotoTOTPScreen()

                } else if (mfaval.connectorId!!.equals("smsConnector",
                        true) || mfaval.connectorId!!.equals("voiceConnector", true)
                ) {
                    if (mfaval.connectionId != null) {
                        BaseApplication.instance().setConnectionid(mfaval.connectionId!!)
                    } else {
                        BaseApplication.instance().setConnectionid(flowData.connectionId!!)
                    }
                    if (mfaval.connectorId!!.equals("voiceConnector", true)) {
                        mActivity.gotoVoiceAuthScreen()
                    } else {
                        mActivity.gotoSmsScreen()
                    }

                } else if (mfaval.connectorId.equals("jumioConnector", true)) {
                    if (mfaval.connectionId != null) {
                        BaseApplication.instance().setConnectionid(mfaval.connectionId!!)
                    } else {
                        BaseApplication.instance().setConnectionid(flowData.connectionId!!)
                    }
                    mActivity.gotoJumioScreen()
                }

            }

            mBinding.llContainer.addView(linearLayout)
        }

    }
}