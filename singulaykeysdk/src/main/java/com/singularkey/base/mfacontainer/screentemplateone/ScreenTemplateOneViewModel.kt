package com.singularkey.base.ui.mfacontainer.screentemplateone

import android.content.Context
import android.graphics.Typeface
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.BaseApplication
import com.singularkey.base.base.BaseViewModel
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.repository.ApiListener
import com.singularkey.base.repository.BaseModuleRepository
import java.util.*

const val BASE64_FLAG = Base64.NO_PADDING or Base64.NO_WRAP or Base64.URL_SAFE

class ScreenTemplateOneViewModel : BaseViewModel<ScreenTemplateOneViewNavigator>() {
    val repository = BaseModuleRepository.instance()
    var configName: String? = ""
    var screenConfig: Screen3ConfigModel? = null
    var userviewIndex: Int = 0
    var isApiCall: Boolean = false
    var capabilityName: String = ""
    var eventName: String = ""
    var title: String = ""
    var iconUrl: String = ""

    fun getFIDOView(ctx: Context, flow: Flow, userViewPosition: Int): ArrayList<View> {
        var viewList = ArrayList<View>()
        if (flow.screen!!.name.equals("MFA Container", true)) {
            for (mfaVal in flow.screen!!.properties!!.mfaList!!.value!!) {
                if (mfaVal.connectorId.equals("fido2Connector", true)) {
                    viewList =
                        getViews(ctx, mfaVal.properties!!, mfaVal.userViews!!, userViewPosition)
                }
            }
        } else if (flow.screen!!.name!!.equals("FIDO2/WebAuthn", true)) {
            viewList = getViews(ctx,
                flow.screen!!.properties!!,
                flow.screen!!.userViews!!,
                userViewPosition)
        }

        return viewList
    }

    private fun getViews(
        ctx: Context,
        properties: ScreenPropertiesModel,
        userViews: ArrayList<UserView>,
        userViewPosition: Int,
    ): ArrayList<View> {
        var viewList = ArrayList<View>()
        configName = userViews[userViewPosition].screenConfigName

        if (configName.equals("screen0Config", true)) {
            screenConfig = properties.screen0Config
        } else if (configName.equals("screen1Config", true)) {
            screenConfig = properties.screen1Config
        } else if (configName.equals("screen2Config", true)) {
            screenConfig = properties.screen2Config
        } else if (configName.equals("screen3Config", true)) {
            screenConfig = properties.screen3Config
        } else if (configName.equals("screen4Config", true)) {
            screenConfig = properties.screen4Config
        } else if (configName.equals("screen5Config", true)) {
            screenConfig = properties.screen5Config
        }

        if (screenConfig != null) {
            if (screenConfig!!.properties!!.nextEvent != null) {
                val constructType = screenConfig!!.properties!!.nextEvent!!.constructType!!
                if (constructType.equals("skEvent", true)) {
                    val eventType = screenConfig!!.properties!!.nextEvent!!.eventType!!
                    if (eventType.equals("post", true)) {
                        eventName = screenConfig!!.properties!!.nextEvent!!.eventName!!
                        isApiCall = true
                    } else if (eventType.equals("nextUserView", true)) {
                        userviewIndex =
                            screenConfig!!.properties!!.nextEvent!!.nextUserViewIndex!!
                    }
                }
            } else if (screenConfig!!.properties!!.onLoadEvent != null) {
                val constructType = screenConfig!!.properties!!.onLoadEvent!!.constructType!!
                if (constructType.equals("skEvent", true)) {
                    val eventType = screenConfig!!.properties!!.onLoadEvent!!.eventType!!
                    if (eventType.equals("post", true)) {
                        eventName = screenConfig!!.properties!!.onLoadEvent!!.eventName!!
                        isApiCall = true
                        //TODO Perfrom Task
                    }
                }
            }
        }
        if (userViews[userViewPosition].items!!.isEmpty()) {
            title = properties.title!!.value!!

            val titleTextBuilder = CustomView.TextViewBuilder(ctx)
                .setText(properties.title!!.value)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTypeFace(Typeface.DEFAULT_BOLD)
                .setTextSize(16f)
                .setGravity(Gravity.CENTER_HORIZONTAL)
            viewList.add(titleTextBuilder.build())
            val descriptionTextBuilder = CustomView.TextViewBuilder(ctx)
                .setText(properties.title!!.value)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTextSize(16f)
                .setGravity(Gravity.CENTER_HORIZONTAL)
            viewList.add(descriptionTextBuilder.build())
        } else {
            for (items in userViews[userViewPosition].items!!) {
                if (items.propertyName.equals("Title", true)) {
                    title = properties.title!!.value!!

                    var titleTextBuilder = CustomView.TextViewBuilder(ctx)
                        .setLayoutParam(LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        ))
                        .setText(properties.title!!.value)
                        .setTypeFace(Typeface.DEFAULT_BOLD)
                        .setTextSize(16f)
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                }
                if (items.propertyName.equals("iconUrlPng", true)) {
                    var layoutParams = LinearLayout.LayoutParams(
                        60,
                        60
                    )
                    layoutParams.gravity = Gravity.CENTER_HORIZONTAL
                    var imageBuilder = CustomView.ImageViewBuilder(ctx)
                        .setLayoutParam(layoutParams)

                    /* val img = ImageView(ctx)
                     img.layoutParams = LinearLayout.LayoutParams(
                         60,
                         60
                     )*/
                    iconUrl = properties.iconUrlPng!!.value!!
                    Glide.with(ctx).load(properties.iconUrlPng!!.value).into(imageBuilder.build())
                    //img.setImageURI(Uri.parse(mfaVal.properties!!.iconUrlPng!!.value))
                    viewList.add(0, imageBuilder.build())
                }
                if (items.propertyName.equals("description", true)) {

                    var layoutParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    layoutParam.topMargin = 10

                    var descriptionTextViewBuilder = CustomView.TextViewBuilder(ctx)
                        .setLayoutParam(layoutParam)
                        .setText(properties.description!!.value)
                        .setTextSize(16f)
                        .setGravity(Gravity.CENTER_HORIZONTAL)
                    /* val tvTitle = TextView(ctx)
                     tvTitle.text = properties.description!!.value
                     var layoutParam = LinearLayout.LayoutParams(
                         LinearLayout.LayoutParams.MATCH_PARENT,
                         LinearLayout.LayoutParams.WRAP_CONTENT
                     )
                     layoutParam.topMargin = 10
                     tvTitle.layoutParams = layoutParam
                     tvTitle.textSize = 16f
                     tvTitle.gravity = Gravity.CENTER_HORIZONTAL*/
                    viewList.add(descriptionTextViewBuilder.build())
                }

                if (items.propertyName.equals("authDescription", true)) {
                    val tvTitle = TextView(ctx)
                    tvTitle.text = properties.authDescription!!.value
                    tvTitle.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    tvTitle.setPadding(16, 16, 16, 16)
                    tvTitle.textSize = 12f
                    tvTitle.gravity = Gravity.CENTER_HORIZONTAL
                    viewList.add(tvTitle)
                }

            }

        }
        return viewList
    }

    fun initFIDORegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.initiateRegistrationResponse!!.challenge!!)
                        var authenticatorAttachement = ""
                        if (response.initiateRegistrationResponse!!.authenticatorSelection != null)
                            authenticatorAttachement =
                                response.initiateRegistrationResponse!!.authenticatorSelection!!.authenticatorAttachment!!

                        val attestation = response.initiateRegistrationResponse!!.attestation
                        Log.d("LOG_TAG", attestation!!)
                        var attestationPreference: AttestationConveyancePreference =
                            AttestationConveyancePreference.NONE
                        if (attestation == "direct") {
                            attestationPreference = AttestationConveyancePreference.DIRECT
                        } else if (attestation == "indirect") {
                            attestationPreference = AttestationConveyancePreference.INDIRECT
                        } else if (attestation == "none") {
                            attestationPreference = AttestationConveyancePreference.NONE
                        }
                        val challenge = Base64.decode(
                            response.initiateRegistrationResponse!!.challenge!!,
                            BASE64_FLAG
                        )

                        navigator.fido2AndroidRegister(
                            response.initiateRegistrationResponse!!.rp!!.name!!,
                            challenge,
                            response.initiateRegistrationResponse!!.user!!.id!!,
                            response.initiateRegistrationResponse!!.user!!.name,
                            authenticatorAttachement,
                            attestationPreference
                        )
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    fun completeFIDORegistration(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.showMessage(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }


    fun initFIDOAuthentication(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: RegistrationInitiationRequest,
    ) {

        getCompositeDisposable().add(
            repository.initAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        var response: AuthenticationRegistrationBaseResponse =
                            successData as AuthenticationRegistrationBaseResponse
                        BaseApplication.instance()
                            .saveChallenge(response.initiateAuthenticationResponse!!.challenge!!)
                        val challenge = Base64.decode(
                            response.initiateAuthenticationResponse!!.challenge!!,
                            BASE64_FLAG
                        )

                        navigator.fido2Auth(
                            response.initiateAuthenticationResponse!!.allowCredentials!!,
                            challenge,
                            response.initiateAuthenticationResponse!!.rpId!!
                        )
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }

    fun completeFIDOAuthentication(
        token: String,
        interactionId: String,
        companyId: String,
        connectionId: String,
        capabilityName: String,
        request: SingularKeyRegistrationCompleteRequest,
    ) {
        getCompositeDisposable().add(
            repository.completeAuthRegistration(
                "Bearer " + token,
                interactionId,
                companyId,
                connectionId,
                capabilityName,
                request,
                object : ApiListener {
                    override fun onSuccess(successData: Any) {
                        navigator.showMessage(successData as AuthenticationRegistrationSuccess)
                    }

                    override fun onError(error: Throwable) {
                        navigator.onError(repository.getErrorMessage(error)!!)
                    }

                }
            )
        )
    }


}