package com.singularkey.base.ui.mfacontainer.jumio

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Parcelable
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.jumio.core.enums.JumioDataCenter
import com.jumio.core.exceptions.MissingPermissionException
import com.jumio.core.exceptions.PlatformNotSupportedException
import com.jumio.nv.NetverifyDeallocationCallback
import com.jumio.nv.NetverifyDocumentData
import com.jumio.nv.NetverifySDK
import com.jumio.nv.data.document.NVDocumentType
import com.singularkey.base.BaseApplication
import com.singularkey.base.BuildConfig
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.SubScreensFragmentBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator

class JumioFragmentScreenOne(
        var mViewModel: SingularKeyViewModel,
) :
        BaseFragment<SubScreensFragmentBinding>(), SingularKeyViewNavigator, NetverifyDeallocationCallback {

    public companion object {
        private const val PERMISSION_REQUEST_CODE_NETVERIFY = 303
    }

    private lateinit var mBinding: SubScreensFragmentBinding
    var flowData: Flow? = null
    private lateinit var mMainFragment: JumioMainFragment

    private lateinit var netverifySDK: NetverifySDK
    private var apiToken: String? = null
    private var apiSecret: String? = null
    private var callbackurl: String? = null
    private var customerInternalReference: String? = null
    private var transactionReference: String? = null

    override fun getLayoutId() = R.layout.sub_screens_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as SubScreensFragmentBinding
        mViewModel.navigator = this
        flowData = BaseApplication.instance().flowData
        mMainFragment = parentFragment as JumioMainFragment
        setupView()

    }


    fun setupView() {
        var viewList = mViewModel.getJumioView(context!!, flowData!!, 0)
        mMainFragment.setTitle(mViewModel.title)
        for (view in viewList) {
            mBinding.llViewContainer.addView(view)
        }
        if (mViewModel.screenConfig != null) {

            var linearLayoutParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT)
            linearLayoutParam.setMargins(20, 20, 20, 20)
            var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                    .setLayoutParam(linearLayoutParam)
                    .setOrientation(LinearLayout.HORIZONTAL)
                    .setPadding(16, 16, 16, 16)
                    .setGravity(Gravity.CENTER)
                    .setCornerRadius(16)
                    .setBackground(Color.parseColor(mViewModel.bgColor))
            var linearLayout = linearLayoutBuilder.build()

            var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
            imageLayoutParams.setMargins(10, 10, 10, 10)
            imageLayoutParams.gravity = Gravity.CENTER_VERTICAL
            var iconBuilder = CustomView.ImageViewBuilder(context!!)
                    .setLayoutParam(imageLayoutParams)

            Glide.with(context!!).load(mViewModel.iconUrl).into(iconBuilder.build())

            var buttonBuilder = CustomView.TextViewBuilder(context!!)
                    .setText(mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!)
                    .setLayoutParam(LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    ))
                    .setTextSize(12f)
                    .setTextColor(resources.getColor(R.color.white))
                    .setBackground(resources.getColor(android.R.color.transparent))

            linearLayout.addView(iconBuilder.build())
            linearLayout.addView(buttonBuilder.build())

            var request = RegistrationInitiationRequest()
            request.capabilityName = flowData!!.capabilityName
            request.connectionId = BaseApplication.instance().connectionId
            if (mViewModel.onLoadEventName.isNotEmpty())
                request.eventName = mViewModel.onLoadEventName
            else
                request.eventName = mViewModel.eventName
            linearLayout.setOnClickListener {
                loadingObservable.value = true
                if (request.eventName.equals("registerInitiate")) {
                    mViewModel.initJumioRegistration(
                            BaseApplication.instance().authToken!!,
                            flowData!!.interactionId!!,
                            flowData!!.companyId!!,
                            BaseApplication.instance().connectionId!!,
                            flowData!!.capabilityName!!,
                            request
                    )
                } else {
                    mViewModel.initJumioAuthInitiation(
                            BaseApplication.instance().authToken!!,
                            flowData!!.interactionId!!,
                            flowData!!.companyId!!,
                            BaseApplication.instance().connectionId!!,
                            flowData!!.capabilityName!!,
                            request
                    )
                }
            }

            mBinding.llViewContainer.addView(linearLayout)
        }
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
            rpname: String,
            challenge: ByteArray,
            userId: String,
            userName: String?,
            authenticatorAttachment: String?,
            attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
            allowCredentials: ArrayList<AllowCredentialModel>,
            challenge: ByteArray,
            rpid: String,
    ) {

    }

    override fun showMessage(msg: String) {

    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {
        loadingObservable.value = false
        apiSecret = response.clientSecret
        apiToken = response.apiKey
        callbackurl = response.webhookUrl
        //callbackurl =
        //  "https://devapi.singularkey.com/v1/auth/T7iaA8e5gz2WB7idjg2k4wYC4xx2Vq4u/connections/c8282bd8e8241729916bb01c98e63ae4/webhooks"
        customerInternalReference = response.customerInternalReference
        transactionReference = response.transactionReference
        BaseApplication.instance().setCreditId(response.credId!!)
        initializeNetverifySDK()

        if (mMainFragment.checkPermissions(PERMISSION_REQUEST_CODE_NETVERIFY)) {
            try {
                if (::netverifySDK.isInitialized) {
                    //view.isEnabled = false
                    startActivityForResult(netverifySDK.intent, NetverifySDK.REQUEST_CODE)
                }
            } catch (e: MissingPermissionException) {
                Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
                //view.isEnabled = true
            }

        }
    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {
        loadingObservable.value = false

        apiSecret = response.clientSecret
        apiToken = response.apiKey
        callbackurl = response.webhookUrl
        //callbackurl =
        //  "https://devapi.singularkey.com/v1/auth/T7iaA8e5gz2WB7idjg2k4wYC4xx2Vq4u/connections/c8282bd8e8241729916bb01c98e63ae4/webhooks"
        customerInternalReference = response.customerInternalReference
        transactionReference = response.transactionReference
        if (response.credId != null)
            BaseApplication.instance().setCreditId(response.credId!!)
        initializeNetverifySDK()

        if (mMainFragment.checkPermissions(PERMISSION_REQUEST_CODE_NETVERIFY)) {
            try {
                if (::netverifySDK.isInitialized) {
                    //view.isEnabled = false
                    startActivityForResult(netverifySDK.intent, NetverifySDK.REQUEST_CODE)
                }
            } catch (e: MissingPermissionException) {
                Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
                //view.isEnabled = true
            }

        }
    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {

        if (response.success!!) {
            if (response.id_token != null) {
                loadingObservable.value = false
                mMainFragment.onSuccessfulRegistration(response)
            } else {
                loadingObservable.value = true
                BaseApplication.instance().saveChallenge(response.challenge!!)
                var request = SingularKeyRegistrationCompleteRequest()
                request.capabilityName = flowData!!.capabilityName
                request.connectionId = BaseApplication.instance().connectionId
                request.eventName = mViewModel.eventName
                request.id = flowData!!.id

                var param = RegistrationCompleteRequestModel()
                if (BaseApplication.instance().credId != null)
                    param.credId = BaseApplication.instance().credId
                if (customerInternalReference != null)
                    param.customerInternalReference = customerInternalReference
                param.transactionReference = transactionReference
                param.challenge = response.challenge
                request.parameters = param
                mViewModel.completeJumioAuthRegistration(BaseApplication.instance().authToken!!,
                        flowData!!.interactionId!!,
                        flowData!!.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData!!.capabilityName!!,
                        request)
            }
        }
    }

    override fun onError(error: BaseError) {
        loadingObservable.value = false
        showError(error.message!!)
        mMainFragment.onFlowError(error)
    }


    private fun initializeNetverifySDK() {
        try {
            // Call the method isSupportedPlatform to check if the device is supported.
            if (!NetverifySDK.isSupportedPlatform(context))
                Log.w("TAG", "Device not supported")

            // Applications implementing the SDK shall not run on rooted devices. Use either the below
            // method or a self-devised check to prevent usage of SDK scanning functionality on rooted
            // devices.
            if (NetverifySDK.isRooted(context))
                Log.w("TAG", "Device is rooted")

            // To create an instance of the SDK, perform the following call as soon as your activity is initialized.
            // Make sure that your merchant API token and API secret are correct and specify an instance
            // of your activity. If your merchant account is created in the EU data center, use
            // JumioDataCenter.EU instead.
            netverifySDK =
                    NetverifySDK.create(mMainFragment.mActivity,
                            apiToken,
                            apiSecret,
                            JumioDataCenter.US)
            // Enable ID verification to receive a verification status and verified data positions (see Callback chapter).
            // Note: Not possible for accounts configured as Fastfill only.
            netverifySDK.setEnableVerification(true)
            val documentTypes = ArrayList<NVDocumentType>()
            documentTypes.add(NVDocumentType.PASSPORT)
            documentTypes.add(NVDocumentType.DRIVER_LICENSE)
            documentTypes.add(NVDocumentType.IDENTITY_CARD)
            netverifySDK.setPreselectedDocumentTypes(documentTypes)
            // The customer internal reference allows you to identify the scan (max. 100 characters).
            // Note: Must not contain sensitive data like PII (Personally Identifiable Information) or account login.
            netverifySDK.setCustomerInternalReference(customerInternalReference)
            // Callback URL for the confirmation after the verification is completed. This setting overrides your Jumio merchant settings.
            netverifySDK.setCallbackUrl(callbackurl)
            Log.e("Callback url: ", callbackurl!!)
            // You can disable Identity Verification during the ID verification for a specific transaction.
            netverifySDK.setEnableIdentityVerification(true)

            var viewList = mViewModel.getJumioView(context!!, flowData!!, 1)

        } catch (e: PlatformNotSupportedException) {
            Log.e("TAG", "Error in initializeNetverifySDK: ", e)
            Toast.makeText(activity?.applicationContext, e.message, Toast.LENGTH_LONG).show()
        } catch (e1: NullPointerException) {
            Log.e("TAG", "Error in initializeNetverifySDK: ", e1)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NetverifySDK.REQUEST_CODE) {

            val scanReference = data?.getStringExtra(NetverifySDK.EXTRA_SCAN_REFERENCE)
            if (resultCode == Activity.RESULT_OK) {
                //Handle the success case and retrieve scan data
                val documentData =
                        data?.getParcelableExtra<Parcelable>(NetverifySDK.EXTRA_SCAN_DATA) as? NetverifyDocumentData
                val mrzData = documentData?.mrzData

                loadingObservable.value = true
                var request = SingularKeyRegistrationCompleteRequest()
                request.capabilityName = flowData!!.capabilityName
                request.connectionId = BaseApplication.instance().connectionId
                request.eventName = mViewModel.eventName
                request.id = flowData!!.id

                var param = RegistrationCompleteRequestModel()
                param.credId = BaseApplication.instance().credId

                param.customerInternalReference = customerInternalReference
                param.transactionReference = transactionReference
                request.parameters = param

                mViewModel.completeJumioAuthRegistration(BaseApplication.instance().authToken!!,
                        flowData!!.interactionId!!,
                        flowData!!.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData!!.capabilityName!!,
                        request)

            } else if (resultCode == Activity.RESULT_CANCELED) {
                //Handle the error cases as highlighted in our documentation: https://github.com/Jumio/mobile-sdk-android/blob/master/docs/integration_faq.md#managing-errors
                val errorMessage = data?.getStringExtra(NetverifySDK.EXTRA_ERROR_MESSAGE)
                val errorCode = data?.getStringExtra(NetverifySDK.EXTRA_ERROR_CODE)
                showError(errorCode + " : " + errorMessage)
            }

            //At this point, the SDK is not needed anymore. It is highly advisable to call destroy(), so that
            //internal resources can be freed.
            netverifySDK.destroy()
            netverifySDK.checkDeallocation(this)
        }
    }

    override fun onNetverifyDeallocated() {

    }

}