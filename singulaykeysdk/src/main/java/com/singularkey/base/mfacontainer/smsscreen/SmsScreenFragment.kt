package com.singularkey.base.ui.mfacontainer.smsscreen

import android.content.Context
import android.view.View
import androidx.databinding.ViewDataBinding
import com.gox.base.extensions.provideViewModel
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.data.model.AuthenticationRegistrationBaseResponse
import com.singularkey.base.data.model.AuthenticationRegistrationSuccess
import com.singularkey.base.data.model.BaseError
import com.singularkey.base.data.model.Flow
import com.singularkey.base.databinding.MainActivityBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyActivity
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.smsscreen.fragments.SmsScreenOneFragment
import com.singularkey.base.ui.mfacontainer.smsscreen.fragments.SmsScreenThreeFragment
import com.singularkey.base.ui.mfacontainer.smsscreen.fragments.SmsScreenTwoFragment

/**
 * Main fragment for sms authentication
 */

class SmsScreenFragment(var isVoice: Boolean) : BaseFragment<MainActivityBinding>() {

    companion object {
        var IS_VOICE: String = "IS_VOICE_SELECTED"
    }

    private lateinit var mBinding: MainActivityBinding
    private lateinit var mViewModel: SingularKeyViewModel
    private lateinit var flowData: Flow
    private lateinit var userName: String
    private lateinit var token: String
    private lateinit var mActivity: SingularKeyActivity


    override fun getLayoutId() = R.layout.main_activity

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as MainActivityBinding
        mViewModel = provideViewModel { SingularKeyViewModel(SingularKeyTone) }

        flowData = BaseApplication.instance().flowData!!
        userName = BaseApplication.instance().username!!
        token = BaseApplication.instance().authToken!!
        mViewModel.isVoice = isVoice
        setupView()
    }

    fun setupView() {
        if (mViewModel.userviewIndex == 0) {
            childFragmentManager.beginTransaction()
                .add(R.id.fragment_container, SmsScreenOneFragment(mViewModel))
                .commitAllowingStateLoss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SingularKeyActivity) {
            mActivity = context as SingularKeyActivity
        }
    }

    fun gotoSecondFragment(response: AuthenticationRegistrationBaseResponse) {
        childFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, SmsScreenTwoFragment(mViewModel, response))
            .commitAllowingStateLoss()
    }

    fun gotoThirdFragment() {
        childFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, SmsScreenThreeFragment(mViewModel))
            .commitAllowingStateLoss()
    }

    fun onSuccessfulRegistration(response: AuthenticationRegistrationSuccess) {
        loadingObservable.value = false
        mActivity.onSuccessfulRegistration(response)
    }

    fun onFlowError(error: BaseError) {
        loadingObservable.value = false
        mActivity.onRegistrationError(error)
    }

    fun setTitle(msg: String) {
        mActivity.setTitle(msg)
    }
}