package com.singularkey.base.ui.mfacontainer.TOTPScreen.fragments

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.SubScreensFragmentBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator
import com.singularkey.base.ui.mfacontainer.TOTPScreen.TOTPScreenMainFragment

class TOTPScreenOneFragment(var mViewModel: SingularKeyViewModel) :
    BaseFragment<SubScreensFragmentBinding>(), SingularKeyViewNavigator {

    private lateinit var mBinding: SubScreensFragmentBinding
    var flowData: Flow? = null
    private lateinit var mFragment: TOTPScreenMainFragment

    override fun getLayoutId() = R.layout.sub_screens_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as SubScreensFragmentBinding
        mViewModel.navigator = this
        flowData = BaseApplication.instance().flowData
        mFragment = parentFragment as TOTPScreenMainFragment
        setupView()
    }

    fun setupView() {
        var viewList = mViewModel.getTOTPView(context!!, flowData!!, 0)
        mFragment.setTitle(mViewModel.title)
        for (view in viewList) {
            mBinding.llViewContainer.addView(view)
        }
        if (mViewModel.screenConfig != null) {

            var linearLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            linearLayoutParam.setMargins(20, 20, 20, 20)
            var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                .setLayoutParam(linearLayoutParam)
                .setOrientation(LinearLayout.HORIZONTAL)
                .setPadding(16, 16, 16, 16)
                .setGravity(Gravity.CENTER)
                .setCornerRadius(16)
                .setBackground(Color.parseColor(mViewModel.bgColor))
            var linearLayout = linearLayoutBuilder.build()

            var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
            imageLayoutParams.setMargins(10, 10, 10, 10)
            imageLayoutParams.gravity = Gravity.CENTER_VERTICAL

            var imageViewBuilder = CustomView.ImageViewBuilder(context!!)
                .setLayoutParam(imageLayoutParams)

            Glide.with(context!!).load(mViewModel.iconUrl).into(imageViewBuilder.build())
            linearLayout.addView(imageViewBuilder.build())

            var buttonViewBuilder = CustomView.TextViewBuilder(context!!)
                .setText(mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTextSize(12f)
                .setTextColor(resources.getColor(R.color.white))
                .setBackground(resources.getColor(android.R.color.transparent))


            linearLayout.addView(buttonViewBuilder.build())

            var request = RegistrationInitiationRequest()
            request.capabilityName = flowData!!.capabilityName
            request.connectionId = BaseApplication.instance().connectionId
            if (mViewModel.onLoadEventName.isNotEmpty())
                request.eventName = mViewModel.onLoadEventName
            else
                request.eventName = mViewModel.eventName


            //btnValue.setBackgroundColor(Color.parseColor(flowData!!.screen!!.metadata!!.colors!!.canvas))

            linearLayout.setOnClickListener {
                loadingObservable.value = true
                if (request.eventName.equals("registerInitiate")) {
                    mViewModel.initTOTPRegistration(
                        BaseApplication.instance().authToken!!,
                        flowData!!.interactionId!!,
                        flowData!!.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData!!.capabilityName!!,
                        request
                    )
                } else {
                    mViewModel.initTOTPAuthInitiation(
                        BaseApplication.instance().authToken!!,
                        flowData!!.interactionId!!,
                        flowData!!.companyId!!,
                        BaseApplication.instance().connectionId!!,
                        flowData!!.capabilityName!!,
                        request
                    )
                }
            }

            mBinding.llViewContainer.addView(linearLayout)
        }
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {

    }

    override fun showMessage(msg: String) {
    }


    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {
        loadingObservable.value = false
        mFragment.gotoSecondFragment(response)
    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {
        loadingObservable.value = false
        mFragment.gotoThirdFragment()
    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {

    }

    override fun onError(msg: BaseError) {
        loadingObservable.value = false
        mFragment.onFlowError(msg)
    }
}