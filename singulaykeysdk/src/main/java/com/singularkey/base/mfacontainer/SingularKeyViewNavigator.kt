package com.singularkey.base.ui.mfacontainer

import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.data.model.*

/**
 *Class which works as interface from view model to view
 * */
interface SingularKeyViewNavigator {
    /**
     * This method is called when flow start api is success and response
     * needs to be transfered to view
     *
     * @see SingularKeyActivity
     *
     * */
    fun onFlowSuccess(response: Flow)

    /**
     * This method is called after successful fido auth registration initialization  and response
     * needs to be transfered to view
     *
     * @see FIDOAuthFragment
     *
     * */
    fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    )

    /**
     * This method is called after successful fido auth login initialization  and response
     * needs to be transfered to view
     *
     * @see FIDOAuthFragment
     *
     * */
    fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    )

    /**
     *
     * Method called from view model to show message
     * */
    fun showMessage(msg: String)

    /**
     * This method is called after successful registration initialization  and response
     * needs to be transfered to view
     *
     * @see JumioFragmentScreenOne, SmsScreenOneFragment....
     *
     * */
    fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse)

    /**
     * This method is called after successful login initialization  and response
     * needs to be transfered to view
     *
     * @see JumioFragmentScreenOne, SmsScreenOneFragment....
     *
     * */
    fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse)

    /**
     * This method is called after successful registration/login authentication  and response
     * needs to be transfered to view
     *
     * @see JumioFragmentScreenOne, SmsScreenOneFragment....
     *
     * */
    fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess)

    /**
     *
     * Method called from view model to show error
     * when received from any api call or validation error
     * */
    fun onError(error: BaseError)
}