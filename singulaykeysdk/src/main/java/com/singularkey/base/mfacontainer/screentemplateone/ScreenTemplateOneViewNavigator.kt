package com.singularkey.base.ui.mfacontainer.screentemplateone

import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.data.model.AllowCredentialModel
import com.singularkey.base.data.model.AuthenticationRegistrationSuccess
import com.singularkey.base.data.model.BaseError

interface ScreenTemplateOneViewNavigator {
    fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    )

    fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    )

    fun showMessage(response: AuthenticationRegistrationSuccess)
    fun onError(error: BaseError)
}