package com.singularkey.base.ui.mfacontainer.smsscreen.fragments

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseFragment
import com.singularkey.base.customviews.CustomView
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.SubScreensFragmentBinding
import com.singularkey.base.ui.mfacontainer.SingularKeyViewModel
import com.singularkey.base.ui.mfacontainer.SingularKeyViewNavigator
import com.singularkey.base.ui.mfacontainer.smsscreen.SmsScreenFragment

class SmsScreenTwoFragment(
    var mViewModel: SingularKeyViewModel,
    var response: AuthenticationRegistrationBaseResponse,
) :
    BaseFragment<SubScreensFragmentBinding>(), SingularKeyViewNavigator {
    var flowData: Flow? = null
    private lateinit var mFragment: SmsScreenFragment
    private lateinit var mBinding: SubScreensFragmentBinding

    override fun getLayoutId() = R.layout.sub_screens_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as SubScreensFragmentBinding
        mViewModel.navigator = this
        flowData = BaseApplication.instance().flowData
        mFragment = parentFragment as SmsScreenFragment
        setupView()
    }

    fun setupView() {
        var viewList = mViewModel.getSmsView(context!!, flowData!!, mViewModel.userviewIndex)
        for (view in viewList) {
            mBinding.llViewContainer.addView(view)
        }
        if (mViewModel.screenConfig != null) {
            if (!mViewModel.screenConfig!!.properties!!.parameters!!.items.isEmpty()) {
                for (items in mViewModel.screenConfig!!.properties!!.parameters!!.items) {

                    var param = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    param.setMargins(10, 10, 10, 10)

                    var editTextBuilder = CustomView.EditTextBuilder(context!!)
                        .setLayoutParam(param)
                        .setHintText("Enter the code")
                        .setPadding(16, 16, 16, 16)
                        .setTag(items as String)

                    mBinding.llViewContainer.addView(editTextBuilder.build())
                }

            }
            var linearLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
            linearLayoutParam.setMargins(20, 20, 20, 20)
            var linearLayoutBuilder = CustomView.LinearLayoutBuilder(context)
                .setLayoutParam(linearLayoutParam)
                .setOrientation(LinearLayout.HORIZONTAL)
                .setPadding(16, 16, 16, 16)
                .setGravity(Gravity.CENTER)
                .setCornerRadius(16)
                .setBackground(Color.parseColor(mViewModel.bgColor))
            var linearLayout = linearLayoutBuilder.build()

            var imageLayoutParams = LinearLayout.LayoutParams(80, 80)
            imageLayoutParams.setMargins(10, 10, 10, 10)
            imageLayoutParams.gravity = Gravity.CENTER_VERTICAL

            var imageViewBuilder = CustomView.ImageViewBuilder(context!!)
                .setLayoutParam(imageLayoutParams)

            Glide.with(context!!).load(mViewModel.iconUrl).into(imageViewBuilder.build())
            linearLayout.addView(imageViewBuilder.build())


            var buttonViewBuilder = CustomView.ButtonBuilder(context!!)
                .setText(mViewModel.screenConfig!!.properties!!.nextButtonText!!.displayName!!)
                .setLayoutParam(LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ))
                .setTextSize(12f)
                .setBackground(resources.getColor(android.R.color.transparent))
                .setTextColor(resources.getColor(R.color.white))

            linearLayout.addView(buttonViewBuilder.build())

            linearLayout.setOnClickListener {
                loadingObservable.value = true
                var request = SingularKeyRegistrationCompleteRequest()
                request.capabilityName = flowData!!.capabilityName
                request.connectionId = BaseApplication.instance().connectionId
                request.eventName = mViewModel.eventName
                request.id = flowData!!.id

                var param = RegistrationCompleteRequestModel()
                param.challenge = BaseApplication.instance().challenge
                param.otp =
                    (mBinding.llViewContainer.getChildAt(mBinding.llViewContainer.childCount - 2)
                            as EditText).text.toString()
                request.parameters = param
                mViewModel.completeSmsAuthRegistration(BaseApplication.instance().authToken!!,
                    flowData!!.interactionId!!,
                    flowData!!.companyId!!,
                    BaseApplication.instance().connectionId!!,
                    flowData!!.capabilityName!!,
                    request)
            }

            mBinding.llViewContainer.addView(linearLayout)
        }
    }

    override fun onFlowSuccess(response: Flow) {

    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {

    }

    override fun showMessage(msg: String) {
    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {
        loadingObservable.value = false
        if (response.success!!) {
            if (response.id_token != null) {
                mFragment.onSuccessfulRegistration(response)
            }
        }
    }

    override fun onError(msg: BaseError) {
        loadingObservable.value = false
        showError(msg.message!!)
        mFragment.onFlowError(msg)
    }
}