package com.singularkey.base.ui.mfacontainer

import android.content.Intent
import androidx.databinding.ViewDataBinding
import com.google.android.gms.fido.fido2.api.common.AttestationConveyancePreference
import com.gox.base.extensions.provideViewModel
import com.jumio.nv.NetverifySDK
import com.singularkey.base.BaseApplication
import com.singularkey.base.R
import com.singularkey.base.base.BaseActivity
import com.singularkey.base.base.SingularKeyTone
import com.singularkey.base.data.model.*
import com.singularkey.base.databinding.MfaScreenBinding
import com.singularkey.base.ui.mfacontainer.TOTPScreen.TOTPScreenMainFragment
import com.singularkey.base.ui.mfacontainer.jumio.JumioMainFragment
import com.singularkey.base.ui.mfacontainer.screentemplateone.FIDOAuthFragment
import com.singularkey.base.ui.mfacontainer.screentemplateone.FIDOAuthFragment.Companion.REQUEST_CODE_REGISTER
import com.singularkey.base.ui.mfacontainer.screentemplateone.FIDOAuthFragment.Companion.REQUEST_CODE_SIGN
import com.singularkey.base.ui.mfacontainer.smsscreen.SmsScreenFragment
import com.singularkey.base.utils.SingularKeyConstantUtils

/**
 * Main Activity of SingularKey library.
 * */
class SingularKeyActivity : BaseActivity<MfaScreenBinding>(), SingularKeyViewNavigator {

    companion object {
        const val USER_NAME = "user_name"
        const val TOKEN = "token"
        const val COMPANY_ID = "company_id"
        const val POLICY_ID = "policy_id"
        const val FLOW_ID = "flow_id"
    }

    private lateinit var mBinding: MfaScreenBinding
    private lateinit var mViewModel: SingularKeyViewModel
    var flowData: Flow? = null
    var userName: String? = null
    var company_id: String? = null
    var flow_id: String? = null
    var policy_id: String? = null

    override fun getLayoutId() = R.layout.mfa_screen

    override fun initView(mViewDataBinding: ViewDataBinding?) {
        mBinding = mViewDataBinding as MfaScreenBinding
        mViewModel = provideViewModel { SingularKeyViewModel(SingularKeyTone) }
        mViewModel.navigator = this

        setSupportActionBar(mBinding.toolbar)
        mBinding.toolbar.setBackgroundColor(mViewModel.getViewData().colorActionBar)
        mBinding.tvTitle.setTextColor(mViewModel.getViewData().colorActionBarTitle)

        supportActionBar!!.setDisplayShowTitleEnabled(false)
        if (intent != null && intent.hasExtra(USER_NAME)) {
            userName = intent.getStringExtra(USER_NAME)
            company_id = intent.getStringExtra(COMPANY_ID)
            flow_id = intent.getStringExtra(FLOW_ID)
            policy_id = intent.getStringExtra(POLICY_ID)
            BaseApplication.instance().setToken(intent.getStringExtra(TOKEN)!!)
            loadingObservable.value = true
            mViewModel.startFlowWithId(company_id!!,
                policy_id,
                flow_id,
                BaseApplication.instance().authToken!!,
                userName!!)
        }

    }

    override fun onFlowSuccess(response: Flow) {
        loadingObservable.value = false
        flowData = response
        BaseApplication.instance().setFlowResponse(flowData!!)
        BaseApplication.instance().setUserName(userName!!)
        BaseApplication.instance().setToken(intent.getStringExtra(TOKEN)!!)
        if (flowData != null) {
            if (flowData!!.screen!!.name!!.equals("MFA Container", true)) {
                supportFragmentManager.beginTransaction().add(R.id.fragment_container,
                    SingularKeyFragment(mViewModel, flowData!!))
                    .commitAllowingStateLoss()
            } else if (flowData!!.screen!!.name!!.equals("FIDO2/WebAuthn", true)) {
                BaseApplication.instance().setConnectionid(flowData!!.connectionId!!)
                supportFragmentManager.beginTransaction().add(R.id.fragment_container,
                    FIDOAuthFragment())
                    .commitAllowingStateLoss()
            } else if (flowData!!.screen!!.name!!.equals("Time based OTP", true)) {
                BaseApplication.instance().setConnectionid(flowData!!.connectionId!!)
                supportFragmentManager.beginTransaction().add(R.id.fragment_container,
                    TOTPScreenMainFragment())
                    .commitAllowingStateLoss()
            } else if (flowData!!.screen!!.name!!.equals("SMS", true)) {
                BaseApplication.instance().setConnectionid(flowData!!.connectionId!!)
                supportFragmentManager.beginTransaction().add(R.id.fragment_container,
                    SmsScreenFragment(false))
                    .commitAllowingStateLoss()
            } else if (flowData!!.screen!!.name!!.equals("Voice OTP", true)) {
                BaseApplication.instance().setConnectionid(flowData!!.connectionId!!)
                supportFragmentManager.beginTransaction().add(R.id.fragment_container,
                    SmsScreenFragment(true))
                    .commitAllowingStateLoss()
            }
        }
    }

    override fun fido2AndroidRegister(
        rpname: String,
        challenge: ByteArray,
        userId: String,
        userName: String?,
        authenticatorAttachment: String?,
        attestationPreference: AttestationConveyancePreference,
    ) {

    }

    override fun fido2Auth(
        allowCredentials: ArrayList<AllowCredentialModel>,
        challenge: ByteArray,
        rpid: String,
    ) {
        TODO("Not yet implemented")
    }

    override fun onError(error: BaseError) {
        showError(error.message!!)
    }

    override fun showMessage(msg: String) {
        showSuccess(msg)
    }

    override fun onRegistrationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthInitiationSuccess(response: AuthenticationRegistrationBaseResponse) {

    }

    override fun onAuthenticationComplete(response: AuthenticationRegistrationSuccess) {

    }

    fun onSuccessfulRegistration(response: AuthenticationRegistrationSuccess) {
        //RETURN to DEMO APP
        var intent = Intent()
        intent.putExtra(SingularKeyConstantUtils.FIDO_RESPONSE_DATA, response)
        setResult(RESULT_OK, intent)
        finish()
    }

    fun onRegistrationError(response: BaseError) {
        //RETURN to DEMO APP
        var intent = Intent()
        intent.putExtra(SingularKeyConstantUtils.FIDO_ERROR_CODE, response.statusCode)
        intent.putExtra(SingularKeyConstantUtils.FIDO_ERROR_MESSAGE, response.message)
        setResult(RESULT_CANCELED, intent)
        finish()
    }

    fun setTitle(title: String) {
        mBinding.tvTitle.text = title

    }

    fun gotoSmsScreen() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,
            SmsScreenFragment(false))
            .commitAllowingStateLoss()
    }

    fun gotoTOTPScreen() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,
            TOTPScreenMainFragment())
            .commitAllowingStateLoss()
    }

    fun gotoVoiceAuthScreen() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,
            SmsScreenFragment(true))
            .commitAllowingStateLoss()
    }

    fun gotoFIDOAuthScreen() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,
            FIDOAuthFragment())
            .commitAllowingStateLoss()
    }

    fun gotoJumioScreen() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container,
            JumioMainFragment())
            .commitAllowingStateLoss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_SIGN -> {
                val currentFragment =
                    supportFragmentManager.findFragmentById(R.id.fragment_container)
                if (currentFragment != null && currentFragment is FIDOAuthFragment) {
                    (currentFragment as FIDOAuthFragment).onActivityResult(requestCode,
                        resultCode,
                        data)
                }
            }
            REQUEST_CODE_REGISTER -> {
                val currentFragment =
                    supportFragmentManager.findFragmentById(R.id.fragment_container)
                if (currentFragment != null && currentFragment is FIDOAuthFragment) {
                    (currentFragment as FIDOAuthFragment).onActivityResult(requestCode,
                        resultCode,
                        data)
                }
            }
            NetverifySDK.REQUEST_CODE -> {
                val currentFragment =
                    supportFragmentManager.findFragmentById(R.id.fragment_container)
                if (currentFragment != null && currentFragment is JumioMainFragment) {
                    (currentFragment as JumioMainFragment).onActivityResult(requestCode,
                        resultCode,
                        data)
                }
            }
        }
    }
}