package com.singularkey.base

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.multidex.MultiDex
import com.singularkey.base.data.Constants
import com.singularkey.base.data.model.Flow
import com.singularkey.base.di.BaseComponent
import com.singularkey.base.di.DaggerBaseComponent
import com.singularkey.base.di.modules.NetworkModule

open class BaseApplication : Application() {
    var flowData: Flow? = null
    var username: String? = null
    var authToken: String? = null
    var connectionId: String? = null
    var challenge: String? = null
    var credId: String? = null
    val baseComponent: BaseComponent = DaggerBaseComponent.builder()
        .networkModule(NetworkModule())
        .build()

    override fun onCreate() {
        super.onCreate()
        baseApplication = this
        preferences = getSharedPreferences(Constants.CUSTOM_PREFERENCE, Context.MODE_PRIVATE)

    }

    fun setFlowResponse(flow: Flow) {
        flowData = flow
    }

    fun setUserName(userName: String) {
        username = userName
    }

    fun setToken(token: String) {
        authToken = token
    }

    fun setCreditId(cred: String) {
        credId = cred
    }

    fun saveChallenge(chal: String) {
        challenge = chal
    }

    fun setConnectionid(connectionid: String) {
        connectionId = connectionid
    }

    fun getToken(): String? {
        return authToken
    }


    companion object {
        lateinit var baseApplication: Context
        private lateinit var preferences: SharedPreferences
        val getCustomPreference: SharedPreferences? get() = preferences
        private var baseinstance: BaseApplication? = null
        fun instance(): BaseApplication {
            if (baseinstance == null) synchronized(BaseApplication) {
                baseinstance = BaseApplication()
            }
            return baseinstance!!
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}