package com.singularkey.base.data.model

class ScreenPropertiesModel {
    var mfaList: MFAListModel? = null
    var title: ViewDetailPropertyModel? = null
    var bodyText: ViewDetailPropertyModel? = null
    var screen3Config: Screen3ConfigModel? = null
    var screen4Config: Screen3ConfigModel? = null
    var screen0Config: Screen3ConfigModel? = null
    var screen1Config: Screen3ConfigModel? = null
    var screen2Config: Screen3ConfigModel? = null
    var screen5Config: Screen3ConfigModel? = null
    var description: ViewDetailPropertyModel? = null
    var authDescription: ViewDetailPropertyModel? = null
    var iconUrl: ViewDetailPropertyModel? = null
    var iconUrlPng: ViewDetailPropertyModel? = null
}