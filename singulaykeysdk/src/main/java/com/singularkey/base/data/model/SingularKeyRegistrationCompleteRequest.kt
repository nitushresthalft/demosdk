package com.singularkey.base.data.model

class SingularKeyRegistrationCompleteRequest {
    var parameters: RegistrationCompleteRequestModel? = null
    var eventName: String? = null
    var connectionId: String? = null
    var capabilityName: String? = null
    var id: String? = null

}