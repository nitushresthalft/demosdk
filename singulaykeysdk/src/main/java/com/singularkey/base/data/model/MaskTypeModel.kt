package com.singularkey.base.data.model

import java.util.ArrayList

class MaskTypeModel {
    var type:String?=null
    var constructType:String?= null
    var displayName:String?=null
    var options:ArrayList<OptionsModel>?=null
    var enum:ArrayList<String>?=null
}