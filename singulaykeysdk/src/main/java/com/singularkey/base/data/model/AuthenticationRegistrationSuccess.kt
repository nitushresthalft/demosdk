package com.singularkey.base.data.model

import java.io.Serializable

class AuthenticationRegistrationSuccess:Serializable {
    var interactionId: String? = null
    var companyId: String? = null
    var connectionId: String? = null
    var connectorId: String? = null
    var id: String? = null
    var capabilityName: String? = null
    var access_token: String? = null
    var token_type: String? = null
    var expires_in: String? = null
    var id_token: String? = null
    var success: Boolean? = null
    var challenge: String? = null
}