package com.singularkey.base.data.model

import java.util.*

class Screen3ConfigModel {
    var type: String? = null
    var constructType: String? = null
    var displayName: String? = null
    var constructItems: ArrayList<String>? = null
    var createdDate: String? = null
    var customerId: String? = null
    var companyId: String? = null
    var properties: Screen3ConfigPropertiesModel? = null
}