package com.singularkey.base.data.model

class RegistrationCompleteRequestModel {
    var webAuthnResponse: WebAuthnResponseData? = null
    var challenge: String? = null
    var otp: String? = null
    var phoneNumber: String? = null
    var credId: String? = null
    var customerInternalReference : String? = null
    var transactionReference : String? = null
}