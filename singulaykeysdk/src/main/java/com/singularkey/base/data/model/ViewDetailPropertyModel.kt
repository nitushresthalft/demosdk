package com.singularkey.base.data.model

import java.util.ArrayList

class ViewDetailPropertyModel {
    var type: String? = null
    var constructType: String? = null
    var displayName: String? = null
    var createdDate: Long? = null
    var customerId: String? = null
    var companyId: String? = null
    var preferredControlType: String? = null
    var value: String? = null
    var userViewPreferredControlType: String? = null
    var css: Any? = null
    var sections:ArrayList<String>?=null
}