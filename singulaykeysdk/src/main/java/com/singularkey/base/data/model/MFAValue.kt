package com.singularkey.base.data.model

import java.util.ArrayList

class MFAValue {
    var connectionId:String?=null
    var connectorId:String?=null
    var properties: ScreenPropertiesModel?=null
    var name:String?=null
    var metadata:MetaData?=null
    var userViews:ArrayList<UserView>?=null
}