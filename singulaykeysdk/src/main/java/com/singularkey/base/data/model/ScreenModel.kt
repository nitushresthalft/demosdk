package com.singularkey.base.data.model

class ScreenModel {
    var name: String? = null
    var properties: ScreenPropertiesModel? = null
    var userViews: ArrayList<UserView>? = null
    var metaData: MetaData? = null
    var connectionId:String?=null
    var connectorId:String?=null
    var metadata:MetaData?=null
}