package com.singularkey.base.data.model

class AuthenticatorSelectionModel {
    var authenticatorAttachment: String? = null
    var userVerification: String? = null
}