package com.gox.base.data

/**
 * This class contains the list of Key that are
 * being used in preference file to save data
 * */
object PreferencesKey {
    const val ACCESS_TOKEN = "access_token"
}