package com.singularkey.base.data.model

import java.util.*

class RegisterInitiationResponse {
    var challenge: String? = null
    var rp: RPModel? = null
    var user: UserModel? = null
    var attestation: String? = null
    var status: String? = null
    var excludeCredentials: ArrayList<Any>? = null
    var errorMessage: String? = null
    var pubKeCredParams: ArrayList<PubKeyCredParamModel>? = null
    var authenticatorSelection: AuthenticatorSelectionModel? = null
}