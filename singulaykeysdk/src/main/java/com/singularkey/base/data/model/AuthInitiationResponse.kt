package com.singularkey.base.data.model

import java.util.*

class AuthInitiationResponse {
    var challenge: String? = null
    var errorMessage: String? = null
    var status: String? = null
    var allowCredentials: ArrayList<AllowCredentialModel>? = null
    var userVerification: String? = null
    var timeout: String? = null
    var rpId: String? = null
}