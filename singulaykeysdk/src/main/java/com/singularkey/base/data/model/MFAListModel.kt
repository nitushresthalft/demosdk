package com.singularkey.base.data.model

import java.util.ArrayList

class MFAListModel {
    var type:String?=null
    var constructType:String?=null
    var displayName:String?=null
    var createdDate:String?=null
    var customerId:String?=null
    var companyId:String?=null
    var value:ArrayList<MFAValue>?=null

}