package com.singularkey.base.data.model

class WebauthResponseModel {
    var attestationObject: String? = null
    var clientDataJSON: String? = null
    var signature: String? = null
    var userHandle: String? = null
    var authenticatorData: String? = null
}