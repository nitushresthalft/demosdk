package com.singularkey.base.data.model

import java.util.*

class UserView {
    var screenTemplateName: String? = null
    var screenConfigName: String? = null
    var items: ArrayList<UserViewItemProperties>? = null
}