package com.singularkey.base.data.model

class AuthenticationRegistrationBaseResponse {
    var interactionId: String? = null
    var companyId: String? = null
    var connectionId: String? = null
    var initiateRegistrationResponse: RegisterInitiationResponse? = null
    var initiateAuthenticationResponse: AuthInitiationResponse? = null
    var capabilityName: String? = null
    var success: Boolean? = null
    var qr: String? = null
    var challenge: String? = null

    var timestamp: String? = null
    var transactionReference: String? = null
    var redirectUrl: String? = null
    var credId: String? = null
    var customerInternalReference: String? = null
    var event: String? = null
    var apiKey: String? = null
    var clientSecret: String? = null
    var webhookUrl: String? = null
}