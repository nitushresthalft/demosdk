package com.singularkey.base.data.model

import java.util.*

class LabelViewDetailModel {
    var type: String? = null
    var nextUserViewIndex: Int? = null
    var constructType: String? = null
    var displayName: String? = null
    var eventName: String? = null
    var eventType: String? = null
    var params: ArrayList<String> = ArrayList<String>()
    var value: Boolean? = null
    var preProcess: ProcessEventModel? = null
    var postProcess: ProcessEventModel? = null
}