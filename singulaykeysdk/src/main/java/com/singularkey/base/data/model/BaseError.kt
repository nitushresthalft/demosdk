package com.singularkey.base.data.model

class BaseError {
    var code: String? = null
    var statusCode: Int? = null
    var message: String? = null
    var httpResponseCode: Int? = null
}