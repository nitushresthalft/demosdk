package com.singularkey.base.data.model

class RegistrationInitiationRequest {
    var eventName: String? = null
    var connectionId: String? = null
    var capabilityName: String? = null
    var parameters: RegistrationCompleteRequestModel? = null
}