package com.singularkey.base.data.model

class Flow {
    var id: String? = null
    var companyId: String? = null
    var flowId: String? = null
    var connectionId: String? = null
    var capabilityName: String? = null
    var screen: ScreenModel? = null
    var interactionId: String? = null
}