package com.singularkey.base.data.model

import org.json.JSONObject

class WebAuthnResponseData {

    var type: String? = null
    var id: String? = null
    var rawId: String? = null
    var getClientExtensionResults = JSONObject()
    var response: WebauthResponseModel? = null
}