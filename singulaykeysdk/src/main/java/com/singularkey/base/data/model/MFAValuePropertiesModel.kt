package com.singularkey.base.data.model

class MFAValuePropertiesModel {
    var screen3Config: Screen3ConfigModel? = null
    var screen0Config: Screen3ConfigModel? = null
    var screen1Config: Screen3ConfigModel? = null
    var screen2Config: Screen3ConfigModel? = null
    var title: ViewDetailPropertyModel? = null
    var description: ViewDetailPropertyModel? = null
    var authDescription: ViewDetailPropertyModel? = null
    var iconUrl: ViewDetailPropertyModel? = null
    var iconUrlPng: ViewDetailPropertyModel? = null
}