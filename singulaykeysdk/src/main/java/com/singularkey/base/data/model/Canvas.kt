package com.singularkey.base.data.model

class Canvas {
    var url: String? = null
    var base64png: String? = null
}