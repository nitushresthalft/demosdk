package com.singularkey.base.data.model

class Screen4ConfigPropertiesModel {
    var title: ViewDetailPropertyModel? = null
    var subTitle: ViewDetailPropertyModel? = null
    var bodyHeaderText: ViewDetailPropertyModel? = null
    var bodyText: ViewDetailPropertyModel? = null
    var nextButtonText: ViewDetailPropertyModel? = null
    var maskType: MaskTypeModel? = null
    var showMask: ViewDetailPropertyModel? = null
    var autoSubmit: ViewDetailPropertyModel? = null
    var bodyIcon0Name: ViewDetailPropertyModel? = null
    var bodyIcon0Link: ViewDetailPropertyModel? = null
    var bodyIcon0LinkPng: ViewDetailPropertyModel? = null
    var showActivityIndicator: ViewDetailPropertyModel? = null
    var bodyIcon1Name: ViewDetailPropertyModel? = null
    var bodyIcon1Link: ViewDetailPropertyModel? = null
    var bodyIcon2Name: ViewDetailPropertyModel? = null
    var bodyIcon2Link: ViewDetailPropertyModel? = null
    var bodyBackgroundTransparent: ViewDetailPropertyModel? = null
    var showPoweredBy: ViewDetailPropertyModel? = null
    var footerLinkText: ViewDetailPropertyModel? = null
    var firstLabel: ViewDetailPropertyModel? = null
    var secondLabel: ViewDetailPropertyModel? = null
    var username: ViewDetailPropertyModel? = null
    var hideButton: ViewDetailPropertyModel? = null
    var formFieldsList: ViewDetailPropertyModel? = null
    var onLoadEvent: LabelViewDetailModel? = null
    var nextEvent: LabelViewDetailModel? = null
    var parameters: ParametersModel? = null
}