package com.singularkey.base.data

import android.Manifest
import android.util.Base64
import  com.singularkey.base.BuildConfig
import  com.singularkey.base.BaseApplication
import java.nio.charset.Charset

/**
 * Class that contains all the constant value that has been used in whole app
 * */
object Constants {

    const val CUSTOM_PREFERENCE: String = "BaseConfigSetting"

}