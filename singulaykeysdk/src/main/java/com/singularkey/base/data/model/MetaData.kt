package com.singularkey.base.data.model

class MetaData {
    var logos: Logos? = null
    var colors: Colors? = null
}