package com.singularkey.base.di;



import com.singularkey.base.di.modules.NetworkModule;
import com.singularkey.base.repository.BaseRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface BaseComponent {
    void inject(BaseRepository respository);
}
