package com.singularkey.base.di.modules;

import androidx.annotation.NonNull;

import com.chuckerteam.chucker.api.ChuckerInterceptor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.singularkey.base.BaseApplication;
import com.singularkey.base.BuildConfig;
import com.singularkey.base.repository.SingularKeyApiConstants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Network Module class to build the retrofit with desired header,
 * response json converter and logger
 **/
@Module
public class NetworkModule {

    @Provides
    @Singleton
    public Retrofit providesRetrofitService(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(SingularKeyApiConstants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public OkHttpClient getHttpClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttp = new OkHttpClient()
                .newBuilder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .addNetworkInterceptor(new AddHeaderInterceptor())
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(interceptor);
        if (BuildConfig.DEBUG)
            okHttp.addInterceptor(new ChuckerInterceptor(BaseApplication.baseApplication));
        return okHttp.build();

    }

    public class AddHeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Content-Type", "application/json");
            builder.addHeader("Accept", "application/json");
            return chain.proceed(builder.build());
        }
    }
}