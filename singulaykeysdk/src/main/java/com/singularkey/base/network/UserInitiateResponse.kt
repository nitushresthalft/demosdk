package com.singularkey.base.network

class UserInitiateResponse {
    var capabilityName: String? = null
    var access_token: String? = null
    var token_type: String? = null
    var expires_in: Long? = null
    var success: Boolean? = false
}