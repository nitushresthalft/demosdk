package com.singularkey.base.views

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.singularkey.base.R
import com.singularkey.base.databinding.CustomDialogBinding

class CustomDialog(context: Context) : Dialog(context) {

    private lateinit var mCustomDialogBinding: CustomDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        mCustomDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.custom_dialog, null, false
        )
        setContentView(mCustomDialogBinding.root)
        //setCancelable(BuildConfig.DEBUG)
        setCancelable(false)
    }
}