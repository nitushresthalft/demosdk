package com.singularkey.base.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

/**
 * Utility class where all the common methods and string are added.
 *
 * */
class SingularKeyConstantUtils {
    companion object {
        const val FIDO_RESPONSE_DATA = "FIDO_RESPONSE_DATA"
        const val FIDO_ERROR_CODE = "FIDO_ERROR_CODE"
        const val FIDO_ERROR_MESSAGE = "FIDO_ERROR_MESSAGE"

        /**
         * Method to convert base64 image string to bitmap
         *
         * @param encodedString: image string in Base64 string
         * @return Bitmap: return the decoded bitmap image
         * */
        fun convertBase64ToImage(encodedString: String): Bitmap {
            var pureBase64Encoded = encodedString.substring(encodedString.indexOf(",") + 1)
            val decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT)
            val decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
            return decodedBitmap
        }
    }


}