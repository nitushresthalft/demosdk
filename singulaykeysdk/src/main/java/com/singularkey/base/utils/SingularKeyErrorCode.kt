package com.singularkey.base.utils

/**
 * Constant class for possible error codes we will received
 *
 * */
class SingularKeyErrorCode {
    companion object {
        public const val ERROR_UNAUTHORIZE = 400
        public const val AUTHENTICATION_ERROR = 401
        public const val ERROR_BAD_REQUEST = 402
        public const val SERIALIZATION_ERROR = 403
        public const val PARSING_ERROR = 404
        public const val SINGULAR_KEY_ERROR = 405
        public const val ERROR_INVALID_RESPONSE = 406
        public const val NETWORK_ERROR = 407
        public const val ERROR_SERVER_EXCEPTION = 409
        public const val ERROR_IO_EXCEPTION = 408
    }

}